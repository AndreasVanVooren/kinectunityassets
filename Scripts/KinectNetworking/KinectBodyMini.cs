using Windows.Kinect;

using DictJointData = System.Collections.Generic.Dictionary<Windows.Kinect.JointType, Kinect.Serialization.KJoint>;

namespace Kinect.Serialization
{
    public class KinectBodyMini
    {
        public byte TrackingId { get; internal set; }
        public DictJointData Joints
        {
            get { return _joints; }
            internal set { _joints = value; }
        }
        internal DictJointData _joints = new DictJointData();

        public Windows.Kinect.Vector4 SpineBaseOrientation { get; internal set; }
        public Windows.Kinect.Vector4 NeckOrientation { get; internal set; }

        public FrameEdges ClippedEdges { get; internal set; }
        public bool IsRestricted { get; internal set; }
        public bool IsTracked { get; internal set; }

        public TrackingConfidence HandLeftConfidence { get; internal set; }
        public HandState HandLeftState { get; internal set; }
        public TrackingConfidence HandRightConfidence { get; internal set; }
        public HandState HandRightState { get; internal set; }

        public static implicit operator KinectBodyMini(Windows.Kinect.Body b)
        {
            //Debug.Log("Converting body");
            if (b == null) return null;
            var k = new KinectBodyMini();
            k.FromKBody(b);
            return k;
        }

        public static implicit operator KinectBodyMini(KinectBody b)
        {
            //Debug.Log("Converting body");
            if (b == null) return null;
            var k = new KinectBodyMini();
            k.FromKBody(b);
            return k;
        }

        public KinectBody ToKBody()
        {
            KinectBody b = new KinectBody();

            b.TrackingId = TrackingId;

            b._joints = _joints;

            b._jointOrientations[JointType.SpineBase].Orientation = SpineBaseOrientation;
            b._jointOrientations[JointType.Neck].Orientation = NeckOrientation;

            b.HandLeftConfidence = HandLeftConfidence;
            b.HandLeftState = HandLeftState;
            b.HandRightConfidence = HandRightConfidence;
            b.HandRightState = HandRightState;

            b.IsTracked = IsTracked;
            b.IsRestricted = IsRestricted;
            b.ClippedEdges = ClippedEdges;

            return b;
        }

        public void FromKBody(KinectBody b)
        {
            TrackingId = (byte)(b.TrackingId & 0xf);

            _joints = b._joints;
            SpineBaseOrientation = b._jointOrientations[JointType.SpineBase].Orientation;
            NeckOrientation = b._jointOrientations[JointType.Neck].Orientation;

            HandLeftConfidence = b.HandLeftConfidence;
            HandLeftState = b.HandLeftState;
            HandRightConfidence = b.HandRightConfidence;
            HandRightState = b.HandRightState;

            IsTracked = b.IsTracked;
            IsRestricted = b.IsRestricted;
            ClippedEdges = b.ClippedEdges;
        }

        public void SwapAllJointsMicro()
        {
            SwapJointsMicro(JointType.ShoulderLeft,     JointType.ShoulderRight);
            SwapJointsMicro(JointType.ElbowLeft,        JointType.ElbowRight);
            SwapJointsMicro(JointType.WristLeft,        JointType.WristRight);
            SwapJointsMicro(JointType.HandLeft,         JointType.HandRight);
            SwapJointsMicro(JointType.ThumbLeft,        JointType.ThumbRight);
            SwapJointsMicro(JointType.HandTipLeft,      JointType.HandTipRight);

            SwapJointsMicro(JointType.HipLeft,          JointType.HipRight);
            SwapJointsMicro(JointType.KneeLeft,         JointType.KneeRight);
            SwapJointsMicro(JointType.AnkleLeft,        JointType.AnkleRight);
            SwapJointsMicro(JointType.FootLeft,         JointType.FootRight);
            
        }                             

        public void SwapJointsMicro( JointType l, JointType r)
        {
            var stateL = Joints[l].TrackingState;
            var posL = Joints[l].Position;

            var stateR = Joints[r].TrackingState;
            var posR = Joints[r].Position;

            Joints[l].TrackingState = stateR;
            Joints[l].Position = posR;

            Joints[r].TrackingState = stateL;
            Joints[r].Position = posL;
        }

        public void SwapJoints()
        {
            for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; ++jt)
            {
                SwapJointWithOtherSide(jt);
            }
        }

        public void SwapJointWithOtherSide(JointType jt)
        {
            KJoint tempJoint = null;
            switch (jt)
            {
                case JointType.ShoulderLeft: case JointType.ShoulderRight:
                    tempJoint = Joints[JointType.ShoulderRight];
                    Joints[JointType.ShoulderRight] = Joints[JointType.ShoulderLeft];
                    Joints[JointType.ShoulderLeft] = tempJoint;
                    return;
                case JointType.ElbowLeft: case JointType.ElbowRight:
                    tempJoint = Joints[JointType.ElbowRight];
                    Joints[JointType.ElbowRight] = Joints[JointType.ElbowLeft];
                    Joints[JointType.ElbowLeft] = tempJoint;
                    return;
                case JointType.WristLeft: case JointType.WristRight:
                    tempJoint = Joints[JointType.WristRight];
                    Joints[JointType.WristRight] = Joints[JointType.WristLeft];
                    Joints[JointType.WristLeft] = tempJoint;
                    return;
                case JointType.HandLeft: case JointType.HandRight:
                    tempJoint = Joints[JointType.HandRight];
                    Joints[JointType.HandRight] = Joints[JointType.HandLeft];
                    Joints[JointType.HandLeft] = tempJoint;
                    return;
                case JointType.HipLeft: case JointType.HipRight:
                    tempJoint = Joints[JointType.HipRight];
                    Joints[JointType.HipRight] = Joints[JointType.HipLeft];
                    Joints[JointType.HipLeft] = tempJoint;
                    return;
                case JointType.KneeLeft: case JointType.KneeRight:
                    tempJoint = Joints[JointType.KneeRight];
                    Joints[JointType.KneeRight] = Joints[JointType.KneeLeft];
                    Joints[JointType.KneeLeft] = tempJoint;
                    break;
                case JointType.AnkleLeft: case JointType.AnkleRight:
                    tempJoint = Joints[JointType.AnkleRight];
                    Joints[JointType.AnkleRight] = Joints[JointType.AnkleLeft];
                    Joints[JointType.AnkleLeft] = tempJoint;
                    break;
                case JointType.FootLeft: case JointType.FootRight:
                    tempJoint = Joints[JointType.FootRight];
                    Joints[JointType.FootRight] = Joints[JointType.FootLeft];
                    Joints[JointType.FootLeft] = tempJoint;
                    break;
                case JointType.HandTipLeft: case JointType.HandTipRight:
                    tempJoint = Joints[JointType.HandTipRight];
                    Joints[JointType.HandTipRight] = Joints[JointType.HandTipLeft];
                    Joints[JointType.HandTipLeft] = tempJoint;
                    break;
                case JointType.ThumbLeft: case JointType.ThumbRight:
                    tempJoint = Joints[JointType.ThumbRight];
                    Joints[JointType.ThumbRight] = Joints[JointType.ThumbLeft];
                    Joints[JointType.ThumbLeft] = tempJoint;
                    break;
                default:
                    break;
            }
        }
    }
}