﻿using UnityEngine;
using System.Collections;

public  class FeatureManager : MonoBehaviour
{

    public static bool UseTempFeatures = true;
    
    public void SetTempFeatures(bool feature)
    {
        UseTempFeatures = feature;
    }
}
