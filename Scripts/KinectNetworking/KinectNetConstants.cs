﻿public class NetConstants
{
    public const short Ping = 1000;   //sent to check if client has data (server side)
    public const short Pong = 1001;   //sent to confirm or deny data availability (client side)

    public const short FrameData = 1005;
    public const short KinectBody = 1006;
    public const short KinectFace = 1007;
    public const short CompressedStream = 1010;

    public const int Port = 4789;
}