﻿using UnityEngine;
using DateTime = System.DateTime;
using System.Collections;
using Kinect.Serialization;
using Windows.Kinect;

namespace Kinect.DataTransfer
{
    public class KinectData
    {
        private bool CheckTurned(ref KinectBodyMini body, ref Windows.Kinect.Vector4? faceQuat)
        {
            //Based on the IsBodyTurned code from the Kinect2Interface
            //this is the comments provided:

            //face = On: Face (357.0/1.0)
            //face = Off
            //|   Head_px <= -0.02
            //|   |   Neck_dx <= 0.08: Face (46.0/1.0)
            //|   |   Neck_dx > 0.08: Back (3.0)
            //|   Head_px > -0.02
            //|   |   SpineShoulder_px <= -0.02: Face (4.0)
            //|   |   SpineShoulder_px > -0.02: Back (64.0/1.0)

            if (faceQuat.HasValue)   //face on
            {
                return false;
            }
            else
            {
                if (body.Joints[JointType.Head].PosRelative.x <= -0.02f)
                {
                    return (body.Joints[JointType.Head].PosDerivative.x > 0.08f);
                }
                else
                {
                    // Head_px > -0.02
                    return (body.Joints[JointType.Head].PosRelative.x > -0.02f);
                }
            }

            //return false;
        }

        private void FixFacing()
        {
            for (int b = 0; b < 6; ++b)
            {
                //data[id].data.bodies[b];
                //if the conditions for the body look like the body is turned away from the camera, apply joint swapping
                var body = data.bodies[b];
                var faceQuat = data.faces[b];

                //invalid, can't deduce without face data, can't work without body data.
                if (body == null || !faceQuat.HasValue) continue;

                if (CheckTurned(ref body, ref faceQuat))
                {
                    //switch joint data
                    body.SwapAllJointsMicro();
                    //fix orientations
                    TrackingConfidence swap = body.HandLeftConfidence;
                    HandState s = body.HandLeftState;
                    body.HandLeftConfidence = body.HandRightConfidence;
                    body.HandLeftState = body.HandRightState;
                    body.HandRightConfidence = swap;
                    body.HandRightState = s;

                    body.SpineBaseOrientation = (Quaternion.Euler(0, 180, 0) * body.SpineBaseOrientation.ToUnityQuat()).ToKVec4();
                    body.NeckOrientation = (Quaternion.Euler(0, 180, 0) * body.NeckOrientation.ToUnityQuat()).ToKVec4();

                    Debug.Log("A body has been turned");

                    //IsFlipped = true;
                }
                //else IsFlipped = false;

            }
        }

        public KinectData(KinectDataMessageCompressed data)
        {
            this.data = data;
            timeReceived = DateTime.Now.Ticks;

            FixFacing();
        }
        public long timeReceived;
        public long previousTimeReceived;
        public KinectDataMessageCompressed data;
        //bool IsFlipped = false;
        internal void Update(KinectData newData)
        {
            previousTimeReceived = timeReceived;
            timeReceived = newData.timeReceived;

            this.data.floorPlane = newData.data.floorPlane;

            KinectBodyMini body = null;

            KinectBodyMini[] newBodies = new KinectBodyMini[6];
            Windows.Kinect.Vector4?[] newFaces = new Windows.Kinect.Vector4?[6];

            for (int i = 0; i < 6; ++i)
            {
                if (newData.data.bodies[i] == null) continue;
                bool alreadyPresent = false;

                for (int j = 0; j < 6; j++)
                {
                    if (data.bodies[j] == null) continue;

                    if (data.bodies[j].TrackingId == newData.data.bodies[i].TrackingId)
                    {
                        alreadyPresent = true;

                        body = data.bodies[j];
                        var newBody = newData.data.bodies[i];

                        UpdateBody(ref body, ref newBody, i, j);

                        newBodies[i] = body;

                        if (data.faces[j] == null)
                            newFaces[i] = data.faces[j];

                        break;
                    }
                }
                if (!alreadyPresent)
                {
                    newBodies[i] = newData.data.bodies[i];
                    if (newData.data.faces[i] != null)
                        newFaces[i] = newData.data.faces[i];
                }
                
            }
            data.bodies = newBodies;

            data.faces = newFaces;
            //newBodies = null;

            FixFacing();
        }

        private void UpdateBody(ref KinectBodyMini body, ref KinectBodyMini newBody, int i, int j)
        {
            //KinectBodyMini 
            UpdateJoints(ref body, ref newBody,
                previousTimeReceived == 0 ? timeReceived - previousTimeReceived : 0);

            body.SpineBaseOrientation = newBody.SpineBaseOrientation;
            body.NeckOrientation = newBody.NeckOrientation;

            body.IsTracked = newBody.IsTracked;
            body.IsRestricted = newBody.IsRestricted;
            body.ClippedEdges = newBody.ClippedEdges;

            body.HandLeftConfidence = newBody.HandLeftConfidence;
            body.HandLeftState = newBody.HandLeftState;
            body.HandRightConfidence = newBody.HandRightConfidence;
            body.HandRightState = newBody.HandRightState;
        }

        private void UpdateJoints(ref KinectBodyMini body, ref KinectBodyMini newBody, long timeInTicks)
        {
            float frameTime = (float)timeInTicks / 100000000000;

            for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; ++jt)
            {
                var joint = body.Joints[jt];
                //previous joint
                joint.PosPrevious = joint.Position;

                joint.Position = newBody.Joints[jt].Position;

                if (frameTime > 0)
                    joint.PosDerivative = (joint.Position.ToVec3() - joint.PosPrevious.ToVec3()) / frameTime;
                else
                    joint.PosDerivative = Vector3.zero;

                var parent = KinectHelper.GetParentJoint(jt);
                if (parent.HasValue)
                {
                    joint.PosRelative = body.Joints[parent.Value].Position.ToVec3() - joint.Position.ToVec3();
                }
                else joint.PosRelative = Vector3.zero;


            }
        }
    }

}
