using UnityEngine;
using Windows.Kinect;
using Vector4 = Windows.Kinect.Vector4;

public static class KinectHelper
{
    public static Vector3 CamSpacePtToVec3(CameraSpacePoint point)
    {
        Vector3 vec;
        vec.x = point.X;
        vec.y = point.Y;
        vec.z = point.Z;
        return vec;
    }

    public static Vector3 ToVec3(this CameraSpacePoint p)
    {
        return CamSpacePtToVec3(p);
    }

    public static Quaternion Vector4ToQuat(Vector4 vec)
    {
        Quaternion quat = new Quaternion();
        quat.x = vec.X;
        quat.y = vec.Y;
        quat.z = vec.Z;
        quat.w = vec.W;
        return quat;
    }

    public static Vector4 QuatToVector4(Quaternion quat)
    {
        Vector4 vec = new Vector4();
        vec.X = quat.x;
        vec.Y = quat.y;
        vec.Z = quat.z;
        vec.W = quat.w;
        return vec;
    }

    public static Quaternion ToUnityQuat(this Vector4 vec)
    {
        return Vector4ToQuat(vec);
    }

    public static Vector4 ToKVec4(this Quaternion vec)
    {
        return QuatToVector4(vec);
    }

    public static readonly Quaternion KinectDefaultRotation = Quaternion.LookRotation(-Vector3.forward);

    //static returning dictionary, based on this explanation : http://stackoverflow.com/a/268223
    public static JointType? GetParentJoint(JointType j)
    {
        switch (j)
        {
            case JointType.SpineBase:
                return null;
            case JointType.SpineMid:
                return JointType.SpineBase;
            case JointType.Neck:
                return JointType.SpineShoulder;
            case JointType.Head:
                return JointType.Neck;
            case JointType.ShoulderLeft:
                return JointType.SpineShoulder;
            case JointType.ElbowLeft:
                return JointType.ShoulderLeft;
            case JointType.WristLeft:
                return JointType.ElbowLeft;
            case JointType.HandLeft:
                return JointType.WristLeft;
            case JointType.ShoulderRight:
                return JointType.SpineShoulder;
            case JointType.ElbowRight:
                return JointType.ShoulderRight;
            case JointType.WristRight:
                return JointType.ElbowRight;
            case JointType.HandRight:
                return JointType.WristRight;
            case JointType.HipLeft:
                return JointType.SpineBase;
            case JointType.KneeLeft:
                return JointType.HipLeft;
            case JointType.AnkleLeft:
                return JointType.KneeLeft;
            case JointType.FootLeft:
                return JointType.AnkleLeft;
            case JointType.HipRight:
                return JointType.SpineBase;
            case JointType.KneeRight:
                return JointType.HipRight;
            case JointType.AnkleRight:
                return JointType.KneeRight;
            case JointType.FootRight:
                return JointType.AnkleRight;
            case JointType.SpineShoulder:
                return JointType.SpineMid;
            case JointType.HandTipLeft:
                return JointType.HandLeft;
            case JointType.ThumbLeft:
                return JointType.WristLeft;
            case JointType.HandTipRight:
                return JointType.HandRight;
            case JointType.ThumbRight:
                return JointType.WristRight;
            default:
                Debug.LogError("Verify switch cases");
                return null;
        }
    }

    public static JointType[] GetDirectChildJoints(JointType j)
    {
        switch (j)
        {
            case JointType.SpineBase:
                return new JointType[] { JointType.SpineMid, JointType.HipLeft, JointType.HipRight };
            case JointType.SpineMid:
                return new JointType[] { JointType.SpineShoulder };
            case JointType.Neck:
                return new JointType[] { JointType.Head };
            case JointType.ShoulderLeft:
                return new JointType[] { JointType.ElbowLeft };
            case JointType.ElbowLeft:
                return new JointType[] { JointType.WristLeft };
            case JointType.WristLeft:
                return new JointType[] { JointType.HandLeft, JointType.ThumbLeft };
            case JointType.HandLeft:
                return new JointType[] { JointType.HandTipLeft };
            case JointType.ShoulderRight:
                return new JointType[] { JointType.ElbowRight };
            case JointType.ElbowRight:
                return new JointType[] { JointType.WristRight };
            case JointType.WristRight:
                return new JointType[] { JointType.HandRight, JointType.ThumbRight };
            case JointType.HandRight:
                return new JointType[] { JointType.HandTipRight };
            case JointType.HipLeft:
                return new JointType[] { JointType.KneeLeft };
            case JointType.KneeLeft:
                return new JointType[] { JointType.AnkleLeft };
            case JointType.AnkleLeft:
                return new JointType[] { JointType.FootLeft };
            case JointType.HipRight:
                return new JointType[] { JointType.KneeRight };
            case JointType.KneeRight:
                return new JointType[] { JointType.AnkleRight };
            case JointType.AnkleRight:
                return new JointType[] { JointType.FootRight };
            case JointType.SpineShoulder:
                return new JointType[] { JointType.Neck, JointType.ShoulderLeft, JointType.ShoulderRight };
            default:
                Debug.Log("This joint has no children, or doesn't exist");
                return null;
        }
    }
}