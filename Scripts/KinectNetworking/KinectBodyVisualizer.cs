﻿using UnityEngine;
using Windows.Kinect;
using Kinect.Serialization;
using System.Collections.Generic;
using System;
using RNG = UnityEngine.Random;

public class KinectBodyVisualizer : MonoBehaviour
{

    public static KinectBodyVisualizer Instance;
    public static readonly Vector3 Displacement = new Vector3(0, 2, -2);

    public Dictionary<ulong, GameObject> objects;
    public Dictionary<ulong, DateTime> lastUpdate;

    public static KinectBodyVisualizer GetInstance_S()
    {
        if (Instance == null)
        {
            GameObject x = new GameObject("KinectBodyVisualizer");
            //Debug.Log("Please wait for visualizer to be created");
            Instance = x.AddComponent<KinectBodyVisualizer>();
        }

        return Instance;
    }

    // Use this for initialization
    void Awake()
    {
        
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
            Instance = this;
            Instance.transform.position = Displacement;
        }
    }

    public void VisualizeBody(KinectBody b)
    {
        if (objects == null)
            objects = new Dictionary<ulong, GameObject>();
        if (lastUpdate == null)
            lastUpdate = new Dictionary<ulong, DateTime>();

        if (b == null || !b.IsTracked)
        {
            Debug.LogWarning("Null passed as value, is this intended?");
            return;
        }

        if (!objects.ContainsKey(b.TrackingId))
        {
            CreateNewBody(b.TrackingId);
        }

        if (!lastUpdate.ContainsKey(b.TrackingId))
        {
            lastUpdate.Add(b.TrackingId, DateTime.Now);
        }
        else
        {
            lastUpdate[b.TrackingId] = DateTime.Now;
        }

        UpdateBodyPosition(b.TrackingId, b);
    }

    public const float BODYSCALE = 1.5f;

    private void UpdateBodyPosition(ulong id, KinectBody b)
    {
        for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; ++jt)
        {
            Transform jointObj = objects[id].transform.FindChild(jt.ToString());
            KJoint curJoint = b.Joints[jt];
            KJointOrientation curOrient = b.JointOrientations[jt];

            jointObj.localPosition = curJoint.UPosKinect * BODYSCALE;
            var kinectRot = curOrient.URotKinect;
            //var facingRot = Quaternion.Euler(0, 180, 0);

            jointObj.localRotation = kinectRot;

        }
    }

    public const float BLOCKSCALE = .1f;

    private void CreateNewBody(ulong id)
    {
        GameObject body = new GameObject(string.Format("Body - {0}", id));
        body.transform.parent = Instance.transform;
        body.transform.localPosition = Vector3.zero;

        var colorVal = new Color(RNG.value, RNG.value, RNG.value);
        for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; ++jt)
        {
            GameObject jointObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            jointObject.transform.localScale = new Vector3(BLOCKSCALE, BLOCKSCALE, BLOCKSCALE);
            jointObject.name = jt.ToString();
            jointObject.transform.parent = body.transform;
            jointObject.GetComponent<Renderer>().material.color = colorVal;
        }
        objects.Add(id, body);

    }

    void OnLevelWasLoaded(int level)
    {
        //Destroy all;
        var it = objects.Values.GetEnumerator();

        for (;it.MoveNext();)
        {
            Destroy(it.Current);
        }

        objects.Clear();
        lastUpdate.Clear();
    }

    List<ulong> obsoleteIds = new List<ulong>();
    // Update is called once per frame
    void Update()
    {
        obsoleteIds.Clear();
        foreach (var item in lastUpdate)
        {
            if(DateTime.Now.Subtract(item.Value) > TimeSpan.FromSeconds(1)) //went a second without update
            {
                obsoleteIds.Add(item.Key);
                Destroy(objects[item.Key]);
            }
        }
        for (int i = 0; i < obsoleteIds.Count; ++i)
        {
            objects.Remove(obsoleteIds[i]);
            lastUpdate.Remove(obsoleteIds[i]);
        }
    }
}
