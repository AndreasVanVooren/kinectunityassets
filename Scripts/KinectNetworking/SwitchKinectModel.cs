﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class SwitchKinectModel : MonoBehaviour
{
    int _currentModelNum = 0;
    public List<GameObject> Models;
    GameObject _currentModelRef;

    [SerializeField] Text _field; 
    // Use this for initialization
    void Start()
    {
        if(Models.Count <= 0)
        {
            Debug.LogError("No models in ModelList");
        }
        else
        {
            InstantiateCurrentModel(0);
        }
    }

    public void PrevModel()
    {
        long id = 0;
        if(_currentModelRef != null)
        {
            
            var controller = _currentModelRef.GetComponent<AvatarController>();
            if(controller == null)
            {
                controller = _currentModelRef.GetComponentInChildren<AvatarController>();
            }

            if(controller != null)
            {
                id = controller.playerId;
            }

            Destroy(_currentModelRef);
        }

        --_currentModelNum;
        if(_currentModelNum < 0)
            _currentModelNum = Models.Count - 1;

        InstantiateCurrentModel(id);
    }

    private void InstantiateCurrentModel(long id)
    {
        _currentModelRef = Instantiate(Models[_currentModelNum]);

        var controller = _currentModelRef.GetComponent<AvatarController>();
        if (controller == null)
        {
            controller = _currentModelRef.GetComponentInChildren<AvatarController>();
        }

        if (controller != null)
        {
            controller.playerId = id;
        }

        if (_field != null)
        {
            _field.text = _currentModelRef.name;
        }
    }

    public void NextModel()
    {
        long id = 0;

        if(_currentModelRef != null)
        {
            var controller = _currentModelRef.GetComponent<AvatarController>();
            if (controller == null)
            {
                controller = _currentModelRef.GetComponentInChildren<AvatarController>();
            }

            if (controller != null)
            {
                id = controller.playerId;
            }

            Destroy(_currentModelRef);
        }

        ++_currentModelNum;
        _currentModelNum %= Models.Count;

        InstantiateCurrentModel(id);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
