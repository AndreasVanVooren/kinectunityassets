﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System.Collections;
using System.Collections.Generic;
using Kinect.DataTransfer;
using Kinect.Serialization;

using Windows.Kinect;
using Microsoft.Kinect.Face;

public class KinectNetSlave : MonoBehaviour
{
    NetworkClient myClient;
    internal string ip = "localhost";
    bool IsConnected = false;

    public bool ShowBodiesOnSlaveScreen = true;

    [SerializeField]
    Text Gui;

    [SerializeField]
    InputField Input;

    [SerializeField]
    Text ButtonText;

    KinectSensor mySensor;
    BodyFrameReader frameReader;
    private Body[] bodyList;

    public List<ulong> idsTracked = new List<ulong>();
    Dictionary<ulong, GameObject> bodyContainer = new Dictionary<ulong, GameObject>();

    public FaceFrameSource[] faceFrameSources = null;
    public FaceFrameReader[] faceFrameReaders = null;
    public FaceFrameResult[] faceFrameResults = null;

    public bool IsAvailable { get { return mySensor != null && mySensor.IsOpen && mySensor.IsAvailable; } }

    void Start()
    {
        Application.runInBackground = true;

        CreateClient();

        //if (Gui != null)
        //{
        //    Gui.transform.parent = Camera.main.transform;
        //    Gui.transform.localPosition = Vector3.zero;
        //}

        if (!OpenSensor())
        {
            Debug.LogError("Can't open kinect sensor");
        }
    }

    public void CreateClient(string newIp)
    {
        ip = newIp;
        CreateClient();
    }

    public void ConnectOrDisconnect()
    {

        if (Input.text != "")
        {
            CreateClient(Input.text);
            Input.text = "";
        }
        else
        {
            if (IsConnected)
            {
                myClient.Disconnect();
                Debug.Log("Manual disconnect from server");
                IsConnected = false;
                ButtonText.text = "Connect";
            }
            else
            {
                CreateClient();
            }
        }
        
    }

    public void CreateClient()
    {
        if (myClient == null)
        {
            //initialize client
            myClient = new NetworkClient();

            myClient.RegisterHandler(MsgType.Connect, OnConnectedClient);
            myClient.RegisterHandler(MsgType.Disconnect, OnDisconnectedClient);
            myClient.RegisterHandler(MsgType.Error, OnConnectionError);
            myClient.RegisterHandler(NetConstants.Ping, CheckKinectAvailable);
        }
        else                //just disconnect
            myClient.Disconnect();

        

        myClient.Connect(ip, NetConstants.Port);
        myClient.SetMaxDelay(0);
        //StartCoroutine(TryReconnect());
    }

    void Reconnect()
    {
        myClient.Connect(ip, NetConstants.Port);
    }

    void Update()
    {
        if (CheckSensorValid())
        {
            KinectDataMessageCompressed msg = null;
            //start stream send bodies
            CheckBodyFrame(ref msg);
            //send ending
            EndCurrentSendings(ref msg);

        }
        else
        {


        }
        if (ShowBodiesOnSlaveScreen)
            UpdateBodyObjects();

        UpdateGui();
    }

    IEnumerator TryReconnect()
    {
        for (;;)
        {
            yield return new WaitForSeconds(3);
            if (!IsConnected)
            {

                Reconnect();
            }
            yield return null;
        }

    }

    private void UpdateGui()
    {
        const string hasKinect = "Kinect is available";
        const string noKinect = "Kinect is not available";

        const string hasConnect = "Connected to server";
        const string noConnect = "No connection to server";

        Gui.text = string.Format("{0} & {1}", IsAvailable ? hasKinect : noKinect, IsConnected ? hasConnect : noConnect);
    }

    bool OpenSensor()
    {
        mySensor = KinectSensor.GetDefault();
        if (mySensor == null)
        {
            Debug.LogError("Error: Kinect could not be found, are the drivers installed");
            return false;
        }


        if (frameReader == null)
        {
            frameReader = mySensor.BodyFrameSource.OpenReader();
            var count = mySensor.BodyFrameSource.BodyCount;
            //frameReader.FrameArrived += OnNewFrame;

            if (faceFrameSources == null || faceFrameReaders == null || faceFrameResults == null)
            {
                faceFrameSources = new FaceFrameSource[count];
                faceFrameReaders = new FaceFrameReader[count];

                for (int i = 0; i < count; ++i)
                {
                    faceFrameSources[i] = FaceFrameSource.Create(mySensor, 0,
                        FaceFrameFeatures.LookingAway |
                        FaceFrameFeatures.RotationOrientation |
                        FaceFrameFeatures.FaceEngagement |
                        FaceFrameFeatures.BoundingBoxInColorSpace);

                    faceFrameReaders[i] = faceFrameSources[i].OpenReader();
                }


                faceFrameResults = new FaceFrameResult[count];
            }

        }

        if (frameReader == null) return false;

        mySensor.Open();
        return true;
    }

    private bool CheckSensorValid()
    {

        //if (mySensor == null)
        //    mySensor = KinectSensor.GetDefault();

        if (!IsAvailable)
        {
            Debug.Log("No kinect");
        }
        return mySensor.IsAvailable;

    }

    void CheckBodyFrame(ref KinectDataMessageCompressed msg)
    {
        //happens in update 
        if (frameReader == null) return;
        var frame = frameReader.AcquireLatestFrame();

        if (frame == null) return;
        using (frame)
        {
            //it's expected to be null, assign new var
            msg = new KinectDataMessageCompressed();
            //Debug.Log("Frame live update");
            //start stream
            //myClient.Send(NetConstants.FrameData, new FrameData(frame.FloorClipPlane, frame.RelativeTime));

            //assign floorplanes
            msg.floorPlane = frame.FloorClipPlane;
            msg.relativeTime = frame.RelativeTime.Ticks;

            if (bodyList == null)
            {
                bodyList = new Body[mySensor.BodyFrameSource.BodyCount];
            }
            frame.GetAndRefreshBodyData(bodyList);
            //send bodies
            SendBodies(ref msg);
        }


        //send faces
        CheckFaceFrames(ref msg);
    }

    private int GetFaceSourceIndex(FaceFrameSource faceFrameSource)
    {
        int index = -1;

        for (int i = 0; i < mySensor.BodyFrameSource.BodyCount; i++)
        {
            if (this.faceFrameSources[i] == faceFrameSource)
            {
                index = i;
                break;
            }
        }

        return index;
    }

    private bool ValidateFaceBox(FaceFrameResult faceResult)
    {
        bool isFaceValid = faceResult != null;

        if (isFaceValid)
        {
            var faceBox = faceResult.FaceBoundingBoxInColorSpace;
            //if (faceBox != null)
            {
                // check if we have a valid rectangle within the bounds of the screen space
                isFaceValid = (faceBox.Right - faceBox.Left) > 0 &&
                    (faceBox.Bottom - faceBox.Top) > 0; // &&
                //faceBox.Right <= this.faceDisplayWidth &&
                //faceBox.Bottom <= this.faceDisplayHeight;
            }
        }

        return isFaceValid;
    }

    void CheckFaceFrames(ref KinectDataMessageCompressed msg)
    {
        if (faceFrameSources == null) return;

        var bodyCount = mySensor.BodyFrameSource.BodyCount;
        for (int i = 0; i < bodyCount; ++i)
        {
            if (faceFrameSources[i] != null)
            {
                if (!faceFrameSources[i].IsTrackingIdValid)
                {
                    faceFrameSources[i].TrackingId = 0;
                }

                if (bodyList[i] != null && bodyList[i].IsTracked)
                {
                    faceFrameSources[i].TrackingId = bodyList[i].TrackingId;
                }
            }
            if (faceFrameReaders[i] != null)
            {
                var frame = faceFrameReaders[i].AcquireLatestFrame();
                if (frame == null) continue;
                using (frame)
                {
                    if (msg == null) return;
                    //get face source index
                    int index = GetFaceSourceIndex(frame.FaceFrameSource);

                    //validate face box
                    //if (frame.FaceFrameResult == null) 
                    //{
                    //    Debug.Log("No faceframeresult");
                    //    continue; 
                    //}

                    if (ValidateFaceBox(frame.FaceFrameResult))
                    {
                        faceFrameResults[index] = frame.FaceFrameResult;
                    }
                    else
                    {
                        faceFrameResults[index] = null;
                        continue;
                    }
                    //if (msg == null) return;

                    msg.faces[i] = faceFrameResults[index].FaceRotationQuaternion;
                    //var msg = new FacialData();
                    //msg.TrackingId = faceFrameResults[index].TrackingId;
                    //msg.RotationQuaternion = faceFrameResults[index].FaceRotationQuaternion;
                    //msg.boundingBoxColSpace = faceFrameResults[index].FaceBoundingBoxInColorSpace;
                    //msg.Engaged = faceFrameResults[index].FaceProperties[FaceProperty.Engaged];
                    //msg.LookingAway = faceFrameResults[index].FaceProperties[FaceProperty.LookingAway];
                    //myClient.Send(NetConstants.KinectFace, msg);   
                }
            }
        }
    }

    void EndCurrentSendings(ref KinectDataMessageCompressed msg)
    {
        if (msg == null) return;
        if (IsConnected)
            myClient.Send(NetConstants.CompressedStream, msg);
    }

    void SendBodies(ref KinectDataMessageCompressed msg)
    {
        if (!IsConnected) return;
        for (int i = 0; i < bodyList.Length; ++i)
        {
            if (bodyList[i] == null || bodyList[i].TrackingId == 0) continue;
            if (msg == null) return;
            //var msg = new KinectMessage(bodyList[i]);
            msg.bodies[i] = bodyList[i];
            //myClient.Send(NetConstants.KinectBody, msg);
        }
    }

    private void CheckKinectAvailable(NetworkMessage netMsg)
    {
        IntegerMessage msg = new IntegerMessage(0);

        if (IsAvailable)
        {
            msg.value = 1;
        }
        myClient.Send(NetConstants.Pong, msg);
    }

    private void OnConnectedClient(NetworkMessage netMsg)
    {
        Debug.Log("Connected to server");
        IsConnected = true;
        ButtonText.text = "Disconnect";
    }
    private void OnDisconnectedClient(NetworkMessage netMsg)
    {
        Debug.Log("Disconnected from server");
        IsConnected = false;
        ButtonText.text = "Connect";
    }
    private void OnConnectionError(NetworkMessage netMsg)
    {
        var msg = netMsg.ReadMessage<ErrorMessage>();
        Debug.LogWarningFormat("Error with connection. Error code {0}", msg.errorCode);
        IsConnected = false;
        ButtonText.text = "Connect";
    }

    void OnApplicationQuit()
    {
        mySensor.Close();
        myClient.Disconnect();
    }

    private void UpdateBodyObjects()
    {

        idsTracked.Clear();

        //get the ids of bodies the kinect received
        if (bodyList == null) return;
        foreach (var body in bodyList)
        {
            idsTracked.Add(body.TrackingId);
        }

        //get the ids of bodies in the container
        List<ulong> idsKnown = new List<ulong>(bodyContainer.Keys);

        //delete bodies no longer tracked
        foreach (var id in idsKnown)
        {
            if (!idsTracked.Contains(id))
            {
                Destroy(bodyContainer[id]);
                bodyContainer.Remove(id);
            }
        }

        //create bodies if necessary
        foreach (var body in bodyList)
        {
            if (body == null || !body.IsTracked) continue;
            var id = body.TrackingId;
            if (!bodyContainer.ContainsKey(id))
            {
                CreateNewBody(id);
            }

            UpdateBodyPosition(id, body);
        }

    }

    static float BODYSCALE = 7.5f;

    private void UpdateBodyPosition(ulong id, KinectBody body)
    {
        for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; ++jt)
        {
            Transform jointObj = bodyContainer[id].transform.FindChild(jt.ToString());
            KJoint curJoint = body.Joints[jt];
            KJointOrientation curOrient = body.JointOrientations[jt];

            jointObj.localPosition = curJoint.UPosKinect * BODYSCALE;
            var kinectRot = curOrient.URotKinect;
            //var facingRot = Quaternion.Euler(0, 180, 0);

            jointObj.localRotation = kinectRot;

        }

    }




    private void CreateNewBody(ulong id)
    {
        GameObject body = new GameObject(string.Format("Body - {0}", id));

        for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; ++jt)
        {
            GameObject jointObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            jointObject.transform.localScale = new Vector3(.3f, .3f, .3f);
            jointObject.name = jt.ToString();
            jointObject.transform.parent = body.transform;
        }
        bodyContainer[id] = body;

    }

}