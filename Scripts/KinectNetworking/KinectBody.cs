﻿using UnityEngine;
using Windows.Kinect;
using eAppearance = Windows.Kinect.Appearance;
using Joint = Windows.Kinect.Joint;
using Vector4 = Windows.Kinect.Vector4;
using System.Collections.Generic;

namespace Kinect.Serialization
{
    //override class for writing functions that can adjust values

    public class KJoint
    {
        public JointType JointType;
        public CameraSpacePoint Position;
        public Vector3 PosRelative;
        public Vector3 PosDerivative;
        public CameraSpacePoint PosPrevious;

        public Vector3 UPosKinect
        {
            get
            {
                return new Vector3(Position.X,
                                   Position.Y,
                                   Position.Z);
            }
        }

        public Vector3 UPosOpposite
        {
            get
            {
                return new Vector3(-Position.X,
                    Position.Y,
                    Position.Z);
            }
        }

        public Vector3 UPosMirroredWorld
        {
            get
            {
                return Quaternion.Inverse(KinectHelper.KinectDefaultRotation) * UPosKinect;
            }
        }

        public Vector3 UPosWorldSpace
        {
            get
            {
                return Quaternion.Inverse(KinectHelper.KinectDefaultRotation) * UPosOpposite;
            }
        }

        public TrackingState TrackingState;

        public static implicit operator KJoint(Joint j)                         //implicit for ease of access
        {
            KJoint k = new KJoint();
            k.JointType = j.JointType;
            k.Position = j.Position;
            k.TrackingState = j.TrackingState;
            return k;
        }
    }

    //override class for writing functions that can adjust values
    public class KJointOrientation
    {
        public JointType JointType;
        public Vector4 Orientation;

        public Quaternion URotKinect
        {
            get
            {
                return new Quaternion(Orientation.X,
                                      Orientation.Y,
                                      Orientation.Z,
                                      Orientation.W);
            }
        }

        public Quaternion URotOpposite
        {
            get
            {
                return new Quaternion(-Orientation.X,
                                      Orientation.Y,
                                      Orientation.Z,
                                      -Orientation.W);
            }
        }

        public Quaternion URotMirroredWorld
        {
            get
            {
                return Quaternion.Inverse(KinectHelper.KinectDefaultRotation) * URotKinect;
            }
        }

        public Quaternion URotWorldSpace
        {
            get
            {
                return Quaternion.Inverse(KinectHelper.KinectDefaultRotation) * URotOpposite;
            }
        }

        public static implicit operator KJointOrientation(JointOrientation j)   //implicit for ease of access
        {
            KJointOrientation k = new KJointOrientation();
            k.JointType = j.JointType;
            k.Orientation = j.Orientation;
            return k;
        }
    }

    //Override class for the Kinect body, 
    //allowing for the serialization and deserialization, 
    //as well as extending it with internal fuctions
    public class KinectBody
    {

        //id
        public ulong TrackingId { get; internal set; }

        //body joints
        public Dictionary<JointType, KJoint> Joints
        {
            get { return _joints; }
            internal set { _joints = value; }
        }

        internal Dictionary<JointType, KJoint> _joints = new Dictionary<JointType, KJoint>();

        public Dictionary<JointType, KJointOrientation> JointOrientations
        {
            get { return _jointOrientations; }
            internal set { _jointOrientations = value; }
        }

        internal Dictionary<JointType, KJointOrientation> _jointOrientations = new Dictionary<JointType, KJointOrientation>();

        public TrackingConfidence HandLeftConfidence { get; internal set; }

        public HandState HandLeftState { get; internal set; }

        public TrackingConfidence HandRightConfidence { get; internal set; }

        public HandState HandRightState { get; internal set; }

        public PointF Lean { get; internal set; }
        public TrackingState LeanTrackingState { get; internal set; }

        //precision
        public FrameEdges ClippedEdges { get; internal set; }

        public bool IsRestricted { get; internal set; }
        public bool IsTracked { get; internal set; }

        //face readings
        public Dictionary<Activity, DetectionResult> Activities
        {
            get { return _activities; }
            internal set { _activities = value; }
        }

        internal Dictionary<Activity, DetectionResult> _activities = new Dictionary<Activity, DetectionResult>();

        public Dictionary<eAppearance, DetectionResult> Appearance
        {
            get { return _appearance; }
            internal set { _appearance = value; }
        }

        internal Dictionary<eAppearance, DetectionResult> _appearance = new Dictionary<eAppearance, DetectionResult>();

        public Dictionary<Expression, DetectionResult> Expressions
        {
            get { return _expressions; }
            internal set { _expressions = value; }
        }

        internal Dictionary<Expression, DetectionResult> _expressions = new Dictionary<Expression, DetectionResult>();

        public DetectionResult Engaged { get; internal set; }

        internal KinectBody()
        {
            //fill with null data
            TrackingId = 0;
            for (var jt = JointType.SpineBase; jt <= JointType.ThumbRight; ++jt)
            {
                Joints.Add(jt, new Joint());
                JointOrientations.Add(jt, new JointOrientation());
            }
            for (var i = Activity.EyeLeftClosed; i <= Activity.LookingAway; ++i)
            {
                Activities.Add(i, DetectionResult.Unknown);
            }
            Appearance.Add(eAppearance.WearingGlasses, DetectionResult.Unknown);
            for (var i = Expression.Neutral; i <= Expression.Happy; ++i)
            {
                Expressions.Add(i, DetectionResult.Unknown);
            }
        }

        public KinectBody(KinectBody b)
        {
            TrackingId = b.TrackingId;

            for (var jt = JointType.SpineBase; jt <= JointType.ThumbRight; ++jt)
            {
                Joints.Add(jt, b.Joints[jt]);
                JointOrientations.Add(jt, b.JointOrientations[jt]);
                //Joints[jt] = b.Joints[jt];
                //JointOrientations[jt] = b.JointOrientations[jt];
            }

            HandLeftConfidence = b.HandLeftConfidence;
            HandLeftState = b.HandLeftState;
            HandRightConfidence = b.HandRightConfidence;
            HandRightState = b.HandRightState;

            Lean = b.Lean;
            LeanTrackingState = b.LeanTrackingState;

            ClippedEdges = b.ClippedEdges;
            IsRestricted = b.IsRestricted;
            IsTracked = b.IsTracked;

            for (var i = Activity.EyeLeftClosed; i <= Activity.LookingAway; ++i)
            {
                Activities.Add(i, b.Activities[i]);
                //Activities[i] = b.Activities[i];
            }

            Appearance.Add(eAppearance.WearingGlasses, b.Appearance[eAppearance.WearingGlasses]);

            for (var i = Expression.Neutral; i <= Expression.Happy; ++i)
            {
                Expressions.Add(i, b.Expressions[i]);
            }

            Engaged = b.Engaged;
        }

        public KinectBody(Body b)
        {
            TrackingId = b.TrackingId;

            for (var jt = JointType.SpineBase; jt <= JointType.ThumbRight; ++jt)
            {
                Joints.Add(jt, b.Joints[jt]);
                JointOrientations.Add(jt, b.JointOrientations[jt]);
                //Joints[jt] = b.Joints[jt];
                //JointOrientations[jt] = b.JointOrientations[jt];
            }

            HandLeftConfidence = b.HandLeftConfidence;
            HandLeftState = b.HandLeftState;
            HandRightConfidence = b.HandRightConfidence;
            HandRightState = b.HandRightState;

            Lean = b.Lean;
            LeanTrackingState = b.LeanTrackingState;

            ClippedEdges = b.ClippedEdges;
            IsRestricted = b.IsRestricted;
            IsTracked = b.IsTracked;

            for (var i = Activity.EyeLeftClosed; i <= Activity.LookingAway; ++i)
            {
                Activities.Add(i, b.Activities[i]);
                //Activities[i] = b.Activities[i];
            }

            Appearance.Add(eAppearance.WearingGlasses, b.Appearance[eAppearance.WearingGlasses]);

            for (var i = Expression.Neutral; i <= Expression.Happy; ++i)
            {
                Expressions.Add(i, b.Expressions[i]);
            }

            Engaged = b.Engaged;
        }

        public static implicit operator KinectBody(Body b)
        {
            //Debug.Log("Converting body");
            if (b.TrackingId == 0) return null;
            KinectBody k = new KinectBody(b);
            return k;
        }

        public Vector3 GetJointPosition(JointType jointType, bool useMirror = true, bool useRotation = true)
        {
            var pos = CamSpacePtToVec3(Joints[jointType].Position);
            if (!useMirror)
                pos.x = -pos.x;

            return pos;
        }

        public static Vector3 CamSpacePtToVec3(CameraSpacePoint point)
        {
            Vector3 vec;
            vec.x = point.X;
            vec.y = point.Y;
            vec.z = point.Z;
            return vec;
        }

        /// <summary>
        /// Transform a body according to its transformative.
        /// </summary>
        /// <param name="k">The transformative to apply on the body.</param>
        /// <returns>Returns itself when k = null, returns a New body otherwise</returns>
        public KinectBody TransformLocToWorld(Transformative k,bool applyFault = true)
        {
            if (k == null) return this;

            var body = new KinectBody(this);

            for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++)
            {
                body.Joints[jt].Position = k.TransformPosLocToWorld(this.Joints[jt].Position, applyFault);
                body.Joints[jt].PosPrevious = k.TransformPosLocToWorld(this.Joints[jt].PosPrevious, applyFault);
                body.Joints[jt].PosRelative = k.TransformPosLocToWorld(this.Joints[jt].PosRelative, applyFault);
                body.Joints[jt].PosDerivative = k.TransformPosLocToWorld(this.Joints[jt].PosDerivative, applyFault);
                body.Joints[jt].TrackingState = this.Joints[jt].TrackingState;
                body.Joints[jt].JointType = jt;
            }

            return body;
        }
    }
}

