﻿using UnityEngine;
using Kinect.Serialization;
using Windows.Kinect;

//alternative for Transform, since I can't create transforms manually grumble grumble
public class Transformative
{
    public bool IsSure = false;

    private Vector3 _prevPosition;
    private Vector3 _position;
    private Quaternion _prevRotation;
    private Quaternion _rotation;

    public Vector3 PositionFault = Vector3.zero;
    public Quaternion RotationFault = Quaternion.identity;

    public float PosSmoothingFactor = 0.5f;
    public float RotSmoothingFactor = 0.3f;

    public Vector3 Position
    {
        get
        {
            return _position;
        }
        set
        {
            _prevPosition = _position;
            _position = Vector3.Lerp(_prevPosition, value, PosSmoothingFactor);
        }
    }
    public Quaternion Rotation
    {
        get
        {
            return _rotation;
        }
        set
        {
            _prevRotation = _rotation;
            _rotation = Quaternion.Lerp(_prevRotation, value, RotSmoothingFactor);
        }
    }

    public Transformative()
    {
        SetToZero();
    }

    public void SetToZero()
    {
        _prevPosition = Vector3.zero;
        _position = Vector3.zero;

        _prevRotation = Quaternion.identity;
        _rotation = Quaternion.identity;

        IsSure = true;
    }

    public Vector3 TransformPosLocToWorld(Vector3 pos, bool applyFault = true)
    {

        var rot = Rotation;
        if (applyFault)
        {
            rot = Quaternion.Inverse(RotationFault) * Rotation;
        }
        Vector3 result = rot * pos;
        result += Position;
        if (applyFault)
        {
            result -= PositionFault;
        }
        return result;
    }

    public Vector3 TransformPosWorldToLoc(Vector3 pos, bool applyFault = true)
    {
        Vector3 result = pos;
        if (applyFault)
        {
            result += PositionFault;
        }

        pos -= Position;
        if(applyFault)
        {
            result = RotationFault * Quaternion.Inverse(Rotation) * result;
        }
        else result = Quaternion.Inverse(Rotation) * result;

        return result;
    }

    public CameraSpacePoint TransformPosLocToWorld(CameraSpacePoint pos, bool applyFault = true)
    {
        var vec3 = TransformPosLocToWorld(pos.ToVec3(), applyFault);
        CameraSpacePoint p = new CameraSpacePoint();
        p.X = vec3.x;
        p.Y = vec3.y;
        p.Z = vec3.z;
        return p;
    }

    public CameraSpacePoint TransformPosWorldToLoc(CameraSpacePoint pos, bool applyFault = true)
    {
        var vec3 = TransformPosWorldToLoc(pos.ToVec3(), applyFault);
        CameraSpacePoint p = new CameraSpacePoint();
        p.X = vec3.x;
        p.Y = vec3.y;
        p.Z = vec3.z;
        return p;
    }

    public Windows.Kinect.Vector4 TransformRotLocToWorld(Windows.Kinect.Vector4 vec, bool applyFault = true)
    {
        return TransformRotLocToWorld(vec.ToUnityQuat(), applyFault).ToKVec4();
    }

    public Windows.Kinect.Vector4 TransformRotWorldToLoc(Windows.Kinect.Vector4 vec, bool applyFault = true)
    {
        return TransformRotWorldToLoc(vec.ToUnityQuat(), applyFault).ToKVec4();
    }

    public Quaternion TransformRotLocToWorld(Quaternion quat, bool applyFault = true)
    {
        //Rotation * quat * Quaternion.Inverse(RotationFault);
        if (applyFault)
            return Quaternion.Inverse(RotationFault) * Rotation * quat;
        else return Rotation * quat;
    }

    public Quaternion TransformRotWorldToLoc(Quaternion quat, bool applyFault = true)
    {
        if (applyFault)
            return RotationFault * Quaternion.Inverse(Rotation)  * quat;
        else return Quaternion.Inverse(Rotation) * quat;
    }

    public KinectBody TransformBodyLocToWorld(KinectBody k,bool applyFault)
    {
        var body = new KinectBody();

        for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++)
        {
            body.Joints[jt].Position = TransformPosLocToWorld(k.Joints[jt].Position, applyFault);
            body.Joints[jt].PosPrevious = TransformPosLocToWorld(k.Joints[jt].PosPrevious, applyFault);
            body.Joints[jt].PosRelative = TransformPosLocToWorld(k.Joints[jt].PosRelative, applyFault);
            body.Joints[jt].PosDerivative = TransformPosLocToWorld(k.Joints[jt].PosDerivative, applyFault);
            body.Joints[jt].TrackingState = k.Joints[jt].TrackingState;
            body.Joints[jt].JointType = jt;
        }

        return body;
    }
}
