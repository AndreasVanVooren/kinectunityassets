﻿using UnityEngine;
using Windows.Kinect;
using Microsoft.Kinect.Face;
using UnityEngine.Networking;

using Kinect.Serialization;

namespace Kinect.DataTransfer
{
    public class FrameData : MessageBase
    {
        public FrameData() { }

        public FrameData(Windows.Kinect.Vector4 x, System.TimeSpan relTime)
        {
            floorPlane = x;
            relativeTime = relTime.Ticks;
        }

        public Windows.Kinect.Vector4 floorPlane;
        public long relativeTime;

        public override void Serialize(NetworkWriter writer)
        {
            writer.Write(floorPlane);
            writer.Write(relativeTime);
        }
        public override void Deserialize(NetworkReader reader)
        {
            Debug.Log("Deserializing FrameData");
            floorPlane = reader.ReadKinectVec4();
            relativeTime = reader.ReadInt64();
        }
    }

    public class KinectMessage : MessageBase
    {
        public const ulong ULONG0 = 0;
        //public const short messageId = 666;

        public KinectBody MyBody = null;

        public KinectMessage(){ }

        public KinectMessage(Body b)
        {
            if (b.TrackingId != 0)
            {
                Debug.Log("Body added");
                MyBody = b;
            }
            else
            {
                Debug.Log("Body was null");
            }
        }
        
        public override void Serialize(NetworkWriter writer)
        {
            if(MyBody == null)
            {
                writer.Write(ULONG0);
                return;
            }
            //base.Serialize(writer);
            writer.Write(MyBody.TrackingId);
            if (MyBody.TrackingId == 0)
            {
                return;
            }

            WriteJointsAndOrientations(writer);
            WriteHandStates(writer);

            writer.Write(MyBody.Lean);
            WriteMisc(writer);

            WriteActivityAppearanceExpression(writer);
        }
        
        public override void Deserialize(NetworkReader reader)
        {
            //base.Deserialize(reader);
            Debug.Log("Deserializing KinectMessage");
            var id = reader.ReadUInt64();
            if (id == ULONG0) return;

            if (MyBody == null) MyBody = new KinectBody();

            MyBody.TrackingId = id;

            ReadJointsAndOrientations(reader);
            ReadHandStates(reader);

            MyBody.Lean = reader.ReadPointF();
            ReadMisc(reader);

            ReadActivityAppearanceExpression(reader);
        }

        private void WriteMisc(NetworkWriter writer)
        {
            byte byteCarrier = 0;
            byteCarrier += (byte)MyBody.LeanTrackingState;
            byteCarrier <<= 4;
            byteCarrier += (byte)MyBody.ClippedEdges;
            byteCarrier <<= 1;
            byteCarrier += (byte)(MyBody.IsRestricted ? 1 : 0);
            byteCarrier <<= 1;
            byteCarrier += (byte)(MyBody.IsTracked ? 1 : 0);
            writer.Write(byteCarrier);
        }

        private void ReadMisc(NetworkReader reader)
        {
            byte byteCarrier = reader.ReadByte();
            MyBody.IsTracked = (byteCarrier & 1) != 0;
            byteCarrier >>= 1;
            MyBody.IsRestricted = (byteCarrier & 1) != 0;
            byteCarrier >>= 1;
            MyBody.ClippedEdges = (FrameEdges)(byteCarrier & 15);
            byteCarrier >>= 4;
            MyBody.LeanTrackingState = (TrackingState)(byteCarrier & 3);
        }

        private void WriteHandStates(NetworkWriter writer)
        {
            byte byteCarrier = 0;
            byteCarrier += (byte)MyBody.HandLeftConfidence;
            byteCarrier <<= 3;

            byteCarrier += (byte)MyBody.HandLeftState;
            byteCarrier <<= 1;

            byteCarrier += (byte)MyBody.HandRightConfidence;
            byteCarrier <<= 3;

            byteCarrier += (byte)MyBody.HandRightState;
            writer.Write(byteCarrier);
        }

        private void ReadHandStates(NetworkReader reader)
        {
            byte byteCarrier = reader.ReadByte();
            MyBody.HandRightState = (HandState)(byteCarrier & 7);
            byteCarrier >>= 3;
            MyBody.HandRightConfidence = (TrackingConfidence)(byteCarrier & 1);
            byteCarrier >>= 1;

            MyBody.HandLeftState = (HandState)(byteCarrier & 7);
            byteCarrier >>= 3;
            MyBody.HandLeftConfidence = (TrackingConfidence)(byteCarrier & 1);
            //byteCarrier >>= 1;
        }

        private void WriteJointsAndOrientations(NetworkWriter writer)
        {
            ulong trackingStateCarrier = 0;
            for (var jt = JointType.SpineBase; jt <= JointType.ThumbRight; ++jt)
            {
                writer.Write(MyBody.Joints[jt].Position);
                writer.Write(MyBody.JointOrientations[jt].Orientation);

                trackingStateCarrier <<= 2;
                trackingStateCarrier += (ulong)MyBody.Joints[jt].TrackingState;
            }
            writer.Write(trackingStateCarrier);
        }

        private void ReadJointsAndOrientations(NetworkReader reader)
        {
            for (var jt = JointType.SpineBase; jt <= JointType.ThumbRight; ++jt)
            {
                MyBody.Joints[jt].JointType = jt;
                MyBody.Joints[jt].Position = reader.ReadCameraSpacePoint();
                MyBody.JointOrientations[jt].JointType = jt;
                MyBody.JointOrientations[jt].Orientation = reader.ReadKinectVec4();
            }

            ulong trackingStateCarrier = reader.ReadUInt64();
            for (var jt = JointType.ThumbRight; jt >= JointType.SpineBase; --jt)
            {
                MyBody.Joints[jt].TrackingState = (TrackingState) (trackingStateCarrier & 3);
                trackingStateCarrier >>=  2;
            }
        }

        private void WriteActivityAppearanceExpression(NetworkWriter writer)
        {
            //convert array to smaller size
            ushort byteCarrier = 0;
            for (var i = Activity.EyeLeftClosed; i <= Activity.LookingAway; ++i)
            {
                //detectionresult is 2bit enum, 
                byteCarrier <<= 2;
                byteCarrier += (ushort)MyBody.Activities[i];
            }//5*2=10byte

            byteCarrier <<= 2;
            byteCarrier += (ushort)MyBody.Appearance[Appearance.WearingGlasses];//10 + 2byte

            for (var i = Expression.Neutral; i <= Expression.Happy; ++i)
            {
                byteCarrier <<= 2;
                byteCarrier += (ushort)MyBody.Expressions[i];
            }//12 + 2*2 = 16byte

            writer.Write(byteCarrier);
        }

        private void ReadActivityAppearanceExpression(NetworkReader reader)
        {
            ushort byteCarrier = reader.ReadUInt16();

            //go in reverse for deserialization
            for (var i = Expression.Happy; i >= Expression.Neutral; --i)
            {
                MyBody.Expressions[i] = (DetectionResult)(byteCarrier & 3);
                byteCarrier >>= 2;
            }//12 + 2*2 = 16byte

            MyBody.Appearance[Appearance.WearingGlasses] = (DetectionResult)(byteCarrier & 3);
            byteCarrier >>= 2;

            //go in reverse for deserialization
            for (var i = Activity.LookingAway; i >= Activity.EyeLeftClosed; --i)
            {
                MyBody.Activities[i] = (DetectionResult)(byteCarrier & 3);
                byteCarrier >>= 2;
            }//12 + 2*2 = 16byte
        }
    }

    public class FacialData : MessageBase
    {
        public ulong TrackingId = 0;

        public Windows.Kinect.Vector4 RotationQuaternion;
        public RectI boundingBoxColSpace;

        public DetectionResult LookingAway;
        public DetectionResult Engaged;

        public FacialData() { }

        public override void Serialize(NetworkWriter writer)
        {
            writer.Write(TrackingId);
            if (TrackingId == 0) return;

            writer.Write(RotationQuaternion);
            writer.Write(boundingBoxColSpace);

            byte carrier = 0;
            carrier |= (byte)LookingAway;
            carrier <<= 4;
            carrier |= (byte)Engaged;
            writer.Write(carrier);
        }

        public override void Deserialize(NetworkReader reader)
        {
            Debug.Log("Deserializing FacialData");
            TrackingId = reader.ReadUInt64();
            if (TrackingId == 0) return;

            RotationQuaternion = reader.ReadKinectVec4();
            boundingBoxColSpace = reader.ReadRectI();

            byte carrier = reader.ReadByte();
            Engaged = (DetectionResult)(carrier & 0xf);
            carrier >>= 4;
            LookingAway = (DetectionResult)(carrier & 0xf);
        }
    }
}