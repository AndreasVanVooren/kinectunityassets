﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

using System.IO;
//using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using ICSharpCode.SharpZipLib.Zip;
using System;

public class KinectDLLLoader : MonoBehaviour
{
    private static KinectDLLLoader _instance = null;

    bool copyLibs = true;

    public int reloads;
    // Use this for initialization
    void Start()
    {
        if(_instance != null)
        {
            Destroy(this.gameObject);
            
        }

        _instance = this;

        DontDestroyOnLoad(this.gameObject);

        bool needsRestart = false;
        _instance.LoadDLLs(_instance.copyLibs, ref needsRestart);
        if (needsRestart)
        {
            _instance.copyLibs = false;
            Application.LoadLevel(Application.loadedLevel);
        }
        //Debug.Log(_instance.reloads);
        
    }
    // Update is called once per frame
    void Update()
    {

    }

    bool LoadDLLs(bool bCopyLibs, ref bool bNeedRestart)
    {
        bool bOneCopied = false, bAllCopied = true;
        string sTargetPath = KinectInterop.GetTargetDllPath(".", KinectInterop.Is64bitArchitecture()) + "/";

        if (!bCopyLibs)
        {
            // check if the native library is there
            string sTargetLib = sTargetPath + "KinectUnityAddin.dll";
            bNeedRestart = false;

            string sZipFileName = !Is64bitArchitecture() ? "KinectV2UnityAddin.x86.zip" : "KinectV2UnityAddin.x64.zip";
            long iTargetSize = GetUnzippedEntrySize(sZipFileName, "KinectUnityAddin.dll");

            System.IO.FileInfo targetFile = new System.IO.FileInfo(sTargetLib);
            return targetFile.Exists && targetFile.Length == iTargetSize;
        }

        if (!Is64bitArchitecture())
        {
            Debug.Log("x32-architecture detected.");

            //KinectInterop.CopyResourceFile(sTargetPath + "KinectUnityAddin.dll", "KinectUnityAddin.dll", ref bOneCopied, ref bAllCopied);

            Dictionary<string, string> dictFilesToUnzip = new Dictionary<string, string>();
            dictFilesToUnzip["KinectUnityAddin.dll"] = sTargetPath + "KinectUnityAddin.dll";
            dictFilesToUnzip["Kinect20.Face.dll"] = sTargetPath + "Kinect20.Face.dll";
            dictFilesToUnzip["KinectFaceUnityAddin.dll"] = sTargetPath + "KinectFaceUnityAddin.dll";
            dictFilesToUnzip["Kinect2SpeechWrapper.dll"] = sTargetPath + "Kinect2SpeechWrapper.dll";
            dictFilesToUnzip["Kinect20.VisualGestureBuilder.dll"] = sTargetPath + "Kinect20.VisualGestureBuilder.dll";
            dictFilesToUnzip["KinectVisualGestureBuilderUnityAddin.dll"] = sTargetPath + "KinectVisualGestureBuilderUnityAddin.dll";
            dictFilesToUnzip["vgbtechs/AdaBoostTech.dll"] = sTargetPath + "vgbtechs/AdaBoostTech.dll";
            dictFilesToUnzip["vgbtechs/RFRProgressTech.dll"] = sTargetPath + "vgbtechs/RFRProgressTech.dll";
            dictFilesToUnzip["msvcp110.dll"] = sTargetPath + "msvcp110.dll";
            dictFilesToUnzip["msvcr110.dll"] = sTargetPath + "msvcr110.dll";

            UnzipResourceFiles(dictFilesToUnzip, "KinectV2UnityAddin.x86.zip", ref bOneCopied, ref bAllCopied);
        }
        else
        {
            Debug.Log("x64-architecture detected.");

            //KinectInterop.CopyResourceFile(sTargetPath + "KinectUnityAddin.dll", "KinectUnityAddin.dll.x64", ref bOneCopied, ref bAllCopied);

            Dictionary<string, string> dictFilesToUnzip = new Dictionary<string, string>();
            dictFilesToUnzip["KinectUnityAddin.dll"] = sTargetPath + "KinectUnityAddin.dll";
            dictFilesToUnzip["Kinect20.Face.dll"] = sTargetPath + "Kinect20.Face.dll";
            dictFilesToUnzip["KinectFaceUnityAddin.dll"] = sTargetPath + "KinectFaceUnityAddin.dll";
            dictFilesToUnzip["Kinect2SpeechWrapper.dll"] = sTargetPath + "Kinect2SpeechWrapper.dll";
            dictFilesToUnzip["Kinect20.VisualGestureBuilder.dll"] = sTargetPath + "Kinect20.VisualGestureBuilder.dll";
            dictFilesToUnzip["KinectVisualGestureBuilderUnityAddin.dll"] = sTargetPath + "KinectVisualGestureBuilderUnityAddin.dll";
            dictFilesToUnzip["vgbtechs/AdaBoostTech.dll"] = sTargetPath + "vgbtechs/AdaBoostTech.dll";
            dictFilesToUnzip["vgbtechs/RFRProgressTech.dll"] = sTargetPath + "vgbtechs/RFRProgressTech.dll";
            dictFilesToUnzip["msvcp110.dll"] = sTargetPath + "msvcp110.dll";
            dictFilesToUnzip["msvcr110.dll"] = sTargetPath + "msvcr110.dll";

            UnzipResourceFiles(dictFilesToUnzip, "KinectV2UnityAddin.x64.zip", ref bOneCopied, ref bAllCopied);
        }

        UnzipResourceDirectory(sTargetPath, "NuiDatabase.zip", sTargetPath + "NuiDatabase");

        bNeedRestart = (bOneCopied && bAllCopied);

        return true;
    }
    //The following code is copied directly from KinectInterop.cs, so that the DLLs are loaded at launch, rather than when you open the Kinect host scene.

    bool Is64bitArchitecture()
    {
        int sizeOfPtr = Marshal.SizeOf(typeof(IntPtr));
        return (sizeOfPtr > 4);
    }

    public static long GetUnzippedEntrySize(string resZipFileName, string sEntryName)
    {
        TextAsset textRes = Resources.Load(resZipFileName, typeof(TextAsset)) as TextAsset;
        if (textRes == null || textRes.bytes.Length == 0)
        {
            return -1;
        }

        // get the resource steam
        MemoryStream memStream = new MemoryStream(textRes.bytes);

        // fix invalid code page 437 error
        ZipConstants.DefaultCodePage = 0;
        long entryFileSize = -1;

        using (ZipInputStream s = new ZipInputStream(memStream))
        {
            ZipEntry theEntry;
            while ((theEntry = s.GetNextEntry()) != null)
            {
                if (theEntry.Name == sEntryName)
                {
                    entryFileSize = theEntry.Size;
                    break;
                }

            }
        }

        // close the resource stream
        memStream.Close();

        return entryFileSize;
    }

    public static bool UnzipResourceFiles(Dictionary<string, string> dictFilesToUnzip, string resZipFileName,
                                          ref bool bOneCopied, ref bool bAllCopied)
    {
        TextAsset textRes = Resources.Load(resZipFileName, typeof(TextAsset)) as TextAsset;
        if (textRes == null || textRes.bytes.Length == 0)
        {
            bOneCopied = false;
            bAllCopied = false;

            return false;
        }

        //Debug.Log("Unzipping " + resZipFileName + "...");

        // get the resource steam
        MemoryStream memStream = new MemoryStream(textRes.bytes);

        // fix invalid code page 437 error
        ZipConstants.DefaultCodePage = 0;

        using (ZipInputStream s = new ZipInputStream(memStream))
        {
            ZipEntry theEntry;
            while ((theEntry = s.GetNextEntry()) != null)
            {
                //Debug.Log(theEntry.Name);

                if (dictFilesToUnzip.ContainsKey(theEntry.Name))
                {
                    string targetFilePath = dictFilesToUnzip[theEntry.Name];

                    string directoryName = Path.GetDirectoryName(targetFilePath);
                    string fileName = Path.GetFileName(theEntry.Name);

                    if (!Directory.Exists(directoryName))
                    {
                        // create directory
                        Directory.CreateDirectory(directoryName);
                    }

                    FileInfo targetFile = new FileInfo(targetFilePath);
                    bool bTargetFileNewOrUpdated = !targetFile.Exists || targetFile.Length != theEntry.Size;

                    if (fileName != string.Empty && bTargetFileNewOrUpdated)
                    {
                        using (FileStream streamWriter = File.Create(targetFilePath))
                        {
                            int size = 2048;
                            byte[] data = new byte[2048];

                            while (true)
                            {
                                size = s.Read(data, 0, data.Length);

                                if (size > 0)
                                {
                                    streamWriter.Write(data, 0, size);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }

                        bool bFileCopied = File.Exists(targetFilePath);

                        bOneCopied = bOneCopied || bFileCopied;
                        bAllCopied = bAllCopied && bFileCopied;
                    }
                }

            }
        }

        // close the resource stream
        memStream.Close();

        return true;
    }

    public static bool UnzipResourceDirectory(string targetDirPath, string resZipFileName, string checkForDir)
    {
        if (checkForDir != string.Empty && Directory.Exists(checkForDir))
        {
            return false;
        }

        TextAsset textRes = Resources.Load(resZipFileName, typeof(TextAsset)) as TextAsset;
        if (textRes == null || textRes.bytes.Length == 0)
        {
            return false;
        }

        Debug.Log("Unzipping " + resZipFileName + "...");

        // get the resource steam
        MemoryStream memStream = new MemoryStream(textRes.bytes);

        // fix invalid code page 437 error
        ZipConstants.DefaultCodePage = 0;

        using (ZipInputStream s = new ZipInputStream(memStream))
        {
            ZipEntry theEntry;
            while ((theEntry = s.GetNextEntry()) != null)
            {
                //Debug.Log(theEntry.Name);

                string directoryName = targetDirPath + Path.GetDirectoryName(theEntry.Name);
                string fileName = Path.GetFileName(theEntry.Name);

                if (!Directory.Exists(directoryName))
                {
                    // create directory
                    Directory.CreateDirectory(directoryName);
                }

                if (fileName != string.Empty && !fileName.EndsWith(".meta"))
                {
                    string targetFilePath = directoryName + "/" + fileName;

                    using (FileStream streamWriter = File.Create(targetFilePath))
                    {
                        int size = 2048;
                        byte[] data = new byte[2048];

                        while (true)
                        {
                            size = s.Read(data, 0, data.Length);

                            if (size > 0)
                            {
                                streamWriter.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }

        // close the resource stream
        memStream.Close();

        return true;
    }
}
