﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Windows.Kinect;
using Microsoft.Kinect.Face;
using Kinect.Serialization;

namespace Kinect.DataTransfer
{
    //Struct that takes 4 floats, or a vector consisting of 4 floats, and reduces it to halfsize.
    public struct Vec4Compressed
    {
        //public const short QUATMULTIPLICATION = 32767;     //32767 max short val / 1 (max quat val)

        public ushort x;
        public ushort y;
        public ushort z;
        public ushort w;

        public Windows.Kinect.Vector4 ToKVector()
        {
            Windows.Kinect.Vector4 result = new Windows.Kinect.Vector4();
            result.X = Mathf.HalfToFloat(this.x);
            result.Y = Mathf.HalfToFloat(this.y);
            result.Z = Mathf.HalfToFloat(this.z);
            result.W = Mathf.HalfToFloat(this.w);

            return result;
        }
        public Quaternion ToQuaternion()
        {
            Quaternion result;
            result.x = Mathf.HalfToFloat(this.x);
            result.y = Mathf.HalfToFloat(this.y);
            result.z = Mathf.HalfToFloat(this.z);
            result.w = Mathf.HalfToFloat(this.w);

            return result;
        }
        public Vec4Compressed(Windows.Kinect.Vector4 quat)
        {
            this.x = Mathf.FloatToHalf(quat.X);
            this.y = Mathf.FloatToHalf(quat.Y);
            this.z = Mathf.FloatToHalf(quat.Z);
            this.w = Mathf.FloatToHalf(quat.W);
        }
        public Vec4Compressed(Quaternion quat)
        {
            this.x = Mathf.FloatToHalf(quat.x);
            this.y = Mathf.FloatToHalf(quat.y);
            this.z = Mathf.FloatToHalf(quat.z);
            this.w = Mathf.FloatToHalf(quat.w);
        }

        public Vec4Compressed(float x, float y, float z, float w)
        {
            this.x = Mathf.FloatToHalf(x);
            this.y = Mathf.FloatToHalf(y);
            this.z = Mathf.FloatToHalf(z);
            this.w = Mathf.FloatToHalf(w);
        }
    }

    

    public class KinectDataMessageCompressed : MessageBase
    {
        //frame data
        public Windows.Kinect.Vector4 floorPlane;
        public long relativeTime;

        //body data * 6
        public const int bodyCount = 6;
        public byte actualCount { get; private set; }

        public KinectBodyMini[] bodies = new KinectBodyMini[bodyCount];
        //per body
        //Is Tracked
        //Is Restricted
        //tracking id
        // joint position * 25 (AS USHORT)
        // tracking confidence (ulong)
        //hand state (left + right)
        //hand confidence (left + right)
        // joint orientation * 1?

        //face data
        public Windows.Kinect.Vector4?[] faces = new Windows.Kinect.Vector4?[bodyCount];
        
        //init function
        public KinectDataMessageCompressed()
        {
            floorPlane.X = 0;
            floorPlane.Y = 0;
            floorPlane.Z = 0;
            floorPlane.W = 0;
            relativeTime = 0;

            if (bodies == null) bodies = new KinectBodyMini[bodyCount];
            if (faces == null) faces = new Windows.Kinect.Vector4?[bodyCount];
            for (int i = 0; i < bodyCount; ++i)
            {
                bodies[i] = null;
                faces[i] = null;
            }
        }

        

        #region Serialization functions
        private void WriteFrameData(NetworkWriter writer)
        {
            writer.Write(new Vec4Compressed(floorPlane));
            //writer.Write(floorPlane);
            writer.Write(relativeTime);
        }

        //Joint Compression value
        const float DEPTHMULTIPLIER = 51;

        void WriteJointCompressed(NetworkWriter writer, KJoint j, ref ulong bitCarrier)
        {
            ushort x = Mathf.FloatToHalf(j.Position.X);
            ushort y = Mathf.FloatToHalf(j.Position.Y);
            byte z = (byte)Mathf.RoundToInt(j.Position.Z * DEPTHMULTIPLIER);
            //do writing
            writer.Write(x);
            writer.Write(y);
            writer.Write(z);

            bitCarrier <<= 2;
            bitCarrier |= (byte)j.TrackingState;
        }

        private void WriteOrientationCompressed(NetworkWriter writer, KinectBodyMini b)
        {
            Vec4Compressed qComp = new Vec4Compressed(b.SpineBaseOrientation);
            writer.Write(qComp);

            writer.Write(new Vec4Compressed(b.NeckOrientation));
        }

        private void WriteDataCompressed(NetworkWriter writer, KinectBodyMini b, ref ulong bitCarrier)
        {
            bitCarrier <<= 1;
            if (b.IsTracked) bitCarrier |= 1;

            bitCarrier <<= 1;
            if (b.IsRestricted) bitCarrier |= 1;

            bitCarrier <<= 4;
            bitCarrier |= (uint)b.ClippedEdges;

            bitCarrier <<= 3;
            bitCarrier |= (uint)b.HandLeftState;

            bitCarrier <<= 3;
            bitCarrier |= (uint)b.HandRightState;

            bitCarrier <<= 1;
            bitCarrier |= (uint)b.HandLeftConfidence;

            bitCarrier <<= 1;
            bitCarrier |= (uint)b.HandRightConfidence;

            writer.Write(bitCarrier);
        }

        void WriteBody(NetworkWriter writer, KinectBodyMini b)
        {
            writer.Write(b.TrackingId);

            ulong bitCarrier = 0;
            for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; ++jt)
            {
                WriteJointCompressed(writer, b.Joints[jt], ref bitCarrier);
            }
            WriteOrientationCompressed(writer, b);
            WriteDataCompressed(writer, b, ref bitCarrier);
        }

        private void WriteFace(NetworkWriter writer, Windows.Kinect.Vector4 quat)
        {
            Vec4Compressed qComp = new Vec4Compressed(quat);
            writer.Write(qComp);
            //writer.Write(quat.Y);
            //writer.Write(quat.Z);
            //writer.Write(quat.W);
        }
        #endregion

        const byte NoFace = 255;
        
        public override void Serialize(NetworkWriter writer)
        {
            //base.Serialize(writer);
            WriteFrameData(writer);

            byte bodyCountReg = 0;
            //byte faceCountReg = 0;
            for (byte i = 0; i < bodyCount; ++i)
            {
                if (bodies[i] != null) ++bodyCountReg;
                //if (faces[i] != null) ++faceCountReg;
            }
            actualCount = bodyCountReg;
            writer.Write(bodyCountReg);
            for (byte i = 0; i < bodyCount; ++i)
            {
                if (bodies[i] == null) continue;
                WriteBody(writer, bodies[i]);

                if (!faces[i].HasValue)
                {
                    writer.Write(NoFace);
                    continue;
                }

                writer.Write(i);
                WriteFace(writer, faces[i].Value);
            }

            //writer.Write(faceCountReg);
            //for (int i = 0; i < bodyCount; ++i)
            //{
            //    if (faces[i] == null) continue;
            //    WriteFace(writer, faces[i].Value);
            //}

        }


        public override void Deserialize(NetworkReader reader)
        {
            //base.Deserialize(reader);
            ReadFrameData(reader);

            byte bodyCountReg = reader.ReadByte();
            //Debug.LogFormat("Body count = {0}", bodyCountReg);
            actualCount = bodyCountReg;
            for (int i = 0; i < bodyCountReg; ++i)
            {
                bodies[i] = ReadBody(reader);

                var check = reader.ReadByte();
                if (check == NoFace)
                {
                    faces[i] = null;
                    continue;
                }
                faces[i] = ReadFace(reader);
            }


        }

        private void ReadFrameData(NetworkReader reader)
        {
            floorPlane = reader.ReadQuatCompressed().ToKVector();
            relativeTime = reader.ReadInt64();
        }
        
        KinectBodyMini ReadBody(NetworkReader reader)
        {
            KinectBodyMini b = new KinectBodyMini();
            b.TrackingId = reader.ReadByte();

            for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; ++jt)
            {
                var joint = ReadJointCompressed(reader);
                joint.JointType = jt;
                b.Joints[jt] = joint;


            }
            ReadOrientationCompressed(reader, ref b);

            var bitCarrier = reader.ReadUInt64();

            ReadDataCompressed(ref bitCarrier, ref b);
            ReadJointTrackingCompressed(ref bitCarrier, ref b);

            return b;

        }

        private void ReadJointTrackingCompressed(ref ulong bitCarrier, ref KinectBodyMini b)
        {
            for (JointType jt = JointType.ThumbRight; jt >= JointType.SpineBase; --jt)
            {
                b.Joints[jt].TrackingState = (TrackingState)(bitCarrier & 3);
            }
        }

        private void ReadOrientationCompressed(NetworkReader reader, ref KinectBodyMini b)
        {
            b.SpineBaseOrientation = reader.ReadQuatCompressed().ToKVector();
            b.NeckOrientation = reader.ReadQuatCompressed().ToKVector();
        }

        private void ReadDataCompressed(ref ulong bitCarrier, ref KinectBodyMini b)
        {
            //bitCarrier <<= 1;
            //if (b.IsTracked) bitCarrier |= 1;

            //bitCarrier <<= 1;
            //if (b.IsRestricted) bitCarrier |= 1;

            //bitCarrier <<= 4;
            //bitCarrier |= (uint)b.ClippedEdges;

            //bitCarrier <<= 3;
            //bitCarrier |= (uint)b.HandLeftState;

            //bitCarrier <<= 3;
            //bitCarrier |= (uint)b.HandRightState;

            //bitCarrier <<= 1;
            //bitCarrier |= (uint)b.HandLeftConfidence;

            //bitCarrier <<= 1;
            //bitCarrier |= (uint)b.HandRightConfidence;

            b.HandRightConfidence = (TrackingConfidence)(bitCarrier & 1);
            bitCarrier >>= 1;

            b.HandLeftConfidence = (TrackingConfidence)(bitCarrier & 1);
            bitCarrier >>= 1;

            b.HandRightState = (HandState)(bitCarrier & 7);
            bitCarrier >>= 3;

            b.HandLeftState = (HandState)(bitCarrier & 7);
            bitCarrier >>= 3;

            b.ClippedEdges = (FrameEdges)(bitCarrier & 0xf);
            bitCarrier >>= 4;

            b.IsRestricted = (bitCarrier & 1) != 0;
            bitCarrier >>= 1;

            b.IsTracked = (bitCarrier & 1) != 0;
            bitCarrier >>= 1;
        }

        

        KJoint ReadJointCompressed(NetworkReader reader)
        {
            KJoint joint = new KJoint();
            ushort x = reader.ReadUInt16();
            ushort y = reader.ReadUInt16();
            byte z = reader.ReadByte();
            joint.Position.X = Mathf.HalfToFloat(x);
            joint.Position.Y = Mathf.HalfToFloat(y);
            joint.Position.Z = ((float)z) / DEPTHMULTIPLIER;

            return joint;
        }

        private Windows.Kinect.Vector4 ReadFace(NetworkReader reader)
        {
            var quatC = reader.ReadQuatCompressed();
            return quatC.ToKVector();

            //quat.Y = reader.ReadSingle();
            //quat.Z = reader.ReadSingle();
            //quat.W = reader.ReadSingle();
        }
        //body orientation?
        //face data
    }
}
