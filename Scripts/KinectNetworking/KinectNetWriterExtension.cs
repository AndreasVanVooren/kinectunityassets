﻿using UnityEngine.Networking;
using Windows.Kinect;
using Microsoft.Kinect.Face;
using Kinect.DataTransfer;

public static class KinectNetWriterExtension
{
    public static void Write(this NetworkWriter writer, Vec4Compressed p)
    {
        writer.Write(p.x);
        writer.Write(p.y);
        writer.Write(p.z);
        writer.Write(p.w);
    }

    public static void Write(this NetworkWriter writer, PointF p)
    {
        writer.Write(p.X);
        writer.Write(p.Y);
    }

    public static void Write(this NetworkWriter writer, CameraSpacePoint p)
    {
        writer.Write(p.X);
        writer.Write(p.Y);
        writer.Write(p.Z);
    }
    public static void Write(this NetworkWriter writer, Vector4 p)
    {
        writer.Write(p.X);
        writer.Write(p.Y);
        writer.Write(p.Z);
        writer.Write(p.W);
    }

    public static void Write(this NetworkWriter writer, RectI p)
    {
        writer.Write(p.Left);
        writer.Write(p.Right);
        writer.Write(p.Top);
        writer.Write(p.Bottom);
    }

    public static Vec4Compressed ReadQuatCompressed(this NetworkReader reader)
    {
        Vec4Compressed p = new Vec4Compressed();
        p.x = reader.ReadUInt16();
        p.y = reader.ReadUInt16();
        p.z = reader.ReadUInt16();
        p.w = reader.ReadUInt16();

        return p;
    }

    public static PointF ReadPointF(this NetworkReader reader)
    {
        PointF p = new PointF();
        p.X = reader.ReadSingle();
        p.Y = reader.ReadSingle();
        return p;
    }

    public static CameraSpacePoint ReadCameraSpacePoint(this NetworkReader reader)
    {
        CameraSpacePoint p = new CameraSpacePoint();
        p.X = reader.ReadSingle();
        p.Y = reader.ReadSingle();
        p.Z = reader.ReadSingle();
        return p;
    }

    public static Vector4 ReadKinectVec4(this NetworkReader reader)
    {
        Vector4 p = new Vector4();
        p.X = reader.ReadSingle();
        p.Y = reader.ReadSingle();
        p.Z = reader.ReadSingle();
        p.W = reader.ReadSingle();
        return p;
    }

    public static RectI ReadRectI(this NetworkReader reader)
    {
        RectI p = new RectI();
        p.Left = reader.ReadInt32();
        p.Right = reader.ReadInt32();
        p.Top = reader.ReadInt32();
        p.Bottom = reader.ReadInt32();

        return p;
    }
}
