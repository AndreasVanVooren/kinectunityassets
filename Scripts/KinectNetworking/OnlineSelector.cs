﻿using UnityEngine;
using System.Collections;

public class OnlineSelector : MonoBehaviour
{
    [SerializeField]GameObject HostPrefab;
    [SerializeField]GameObject SlavePrefab;

    [SerializeField]UnityEngine.UI.InputField IpField;

    [SerializeField]
    UnityEngine.UI.Toggle TempToggle;

    public void HostServer()
    {
        Application.LoadLevel(1);
    }

    public void JoinServer()
    {
        if (IpField == null || IpField.text.Equals("")) JoinLocalHost();
        else JoinServer(IpField.text);
    }

    public void JoinLocalHost()
    {
        JoinServer("localhost");
    }

    public void JoinServer(string ip)
    {
        if (ip == "") ip = "localhost";

        var obj = Instantiate(SlavePrefab) as GameObject;
        var slave = obj.GetComponent<KinectNetSlave>();
        slave.ip = ip;

        Destroy(this.gameObject);
    }
    
}
