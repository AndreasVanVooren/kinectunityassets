﻿//unity includes
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

//global includes
using System.Collections.Generic;
using System.Collections;
using System;
using Windows.Kinect;

//local includes
using Kinect.Serialization;
using Kinect.DataTransfer.Buffering;

//typedefs
using DictData = System.Collections.Generic.Dictionary<int, Kinect.DataTransfer.KinectData>;

namespace Kinect.DataTransfer
{


    namespace Buffering
    {
        public enum PassResult
        {
            Success,
            AlreadyContained,
            BufferLocked,
            Error,
        }

        public class KinectDataBuffer
        {


            DictData dictReceivedData = new DictData();

            public PassResult PassData(int id, KinectData data, bool replaceIfExists = false)
            {
                if (Locked) return PassResult.BufferLocked;

                try
                {
                    if (!dictReceivedData.ContainsKey(id))
                    {
                        dictReceivedData.Add(id, data);
                        return PassResult.Success;

                    }
                    else if (replaceIfExists)
                    {
                        dictReceivedData[id] = data;
                        return PassResult.Success;

                    }
                    else return PassResult.AlreadyContained;

                }
                catch (Exception)
                {
                    return PassResult.Error;
                }


            }

            public DictData GetData()
            {
                var data = new DictData(dictReceivedData);
                DumpData();
                return data;
            }

            public DictData PeekData()
            {
                return dictReceivedData;
            }

            public void DumpData()
            {
                dictReceivedData.Clear();
                Locked = false;
            }

            public bool Locked { get; private set; }

            public int DataCount { get { return dictReceivedData.Keys.Count; } }
        }

        public class KinectDataBufferHandler
        {
            //KinectDataBuffer _main;
            //KinectDataBuffer _second;

            //KinectDataBuffer _past;

            //List<int> connectionIds = new List<int>();
            DictData connectionData = new DictData();

            //public void AddConnection(int c)
            //{
            //    if (!connectionIds.Contains(c))
            //        connectionIds.Add(c);
            //}
            //public void RemoveConnection(int c)
            //{
            //    if (connectionIds.Contains(c))
            //        connectionIds.Remove(c);
            //}

            public void PassData(int connectionId, KinectData data)
            {
                //AddConnection(connectionId);

                if (!connectionData.ContainsKey(connectionId))
                {
                    Debug.LogFormat("Adding new connection (ID : {0}) and data", connectionId);
                    connectionData.Add(connectionId, data);
                }
                else if (connectionData[connectionId] == null)
                {
                    Debug.LogFormat("Adding new data (ID : {0})", connectionId);
                    connectionData[connectionId] = data;
                }
                else
                {
                    Debug.LogFormat("Updating data (ID : {0})", connectionId);
                    connectionData[connectionId].Update(data);
                }

                //var result = _main.PassData(connectionId, data);
                //switch (result)
                //{
                //    case PassResult.Success:
                //        break;
                //    case PassResult.AlreadyContained:
                //        break;
                //    case PassResult.BufferLocked:
                //        break;
                //    case PassResult.Error:
                //        break;
                //    default:
                //        break;
                //}
            }

            public const long secondInTicks = 10000000;
            public const long obsoletionDelay = secondInTicks / 30 * 4;

            public DictData GetData()
            {
                DictData result = new DictData(connectionData);

                List<int> keysToRemove = new List<int>(result.Count);

                foreach (var pair in result)
                {
                    //remove messages which are considered obsolete, obsoletion happens with 4/30 of a second old frames
                    var now = DateTime.Now;
                    //Debug.LogFormat("Now : {0}, reception : {1}.{2}", now.Ticks,now.Second, now.Millisecond);
                    if (now.Ticks - pair.Value.timeReceived > obsoletionDelay)
                    {
                        keysToRemove.Add(pair.Key);
                        Debug.LogFormat("Removing data (ID : {0})", pair.Key);
                    }
                }

                for (int i = 0; i < keysToRemove.Count; ++i)
                {
                    connectionData.Remove(keysToRemove[i]);
                    result.Remove(keysToRemove[i]);
                }


                return result;
            }

            //public delegate void FrameCompletion(DictData data);
            //public event FrameCompletion OnFrameComplete;
        }
    }



    public class KinectNetReceiver : MonoBehaviour
    {
        bool allowMultipleBodies = false;   //setting this to false will disallow more than one body to be tracked.
        bool allowMultipleSensors = true;   //setting this to false will disallow more than one sensor to be connected to the program. Use only in debug cases.

        public List<int> iConnectedClientsWithData = new List<int>();

        public GUIText GUI;

        int _mainClientId = -1;

        bool _hasFrame = false;

        bool hasFrame
        {
            get { return _hasFrame; }
            set
            {
                _hasFrame = value;
                if (_hasFrame)
                {
                    //Debug.Log("New frame");
                }
            }
        }
        bool _isLocked = false;

        KinectDataBufferHandler _handler;
        Dictionary<int, Transformative> _kinectTransformations;

        public List<KinectBody> _finalKinectBodies = new List<KinectBody>();

        public int ConnectionCount { get { return iConnectedClientsWithData.Count; } }

        public long previousTime;
        public long relativeTime;

        // Use this for initialization
        void Start()
        {
            if (GUI != null)
            {
                GUI.anchor = TextAnchor.LowerLeft;
            }
            LaunchHost();
            previousTime = DateTime.Now.Ticks;
            relativeTime = previousTime;

            Application.runInBackground = true;
        }

        void OnApplicationQuit()
        {
            NetworkServer.Shutdown();
        }

        private void LaunchHost()
        {
            NetworkServer.Listen(NetConstants.Port);
            NetworkServer.RegisterHandler(MsgType.Connect, OnConnect);
            NetworkServer.RegisterHandler(MsgType.Disconnect, OnDisconnect);
            NetworkServer.RegisterHandler(MsgType.Error, OnConnectError);
            NetworkServer.RegisterHandler(NetConstants.CompressedStream, HandleDataStream);
            NetworkServer.RegisterHandler(NetConstants.Pong, ProcessPong);
            NetworkServer.maxDelay = 0;
        }



        private void OnConnect(NetworkMessage netMsg)
        {
            Debug.LogFormat("ConnectionId {0} connected", netMsg.conn.connectionId);
        }

        private void OnDisconnect(NetworkMessage netMsg)
        {
            Debug.LogFormat("ConnectionId {0} disconnected", netMsg.conn.connectionId);
        }

        private void OnConnectError(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<ErrorMessage>();
            Debug.LogWarningFormat("Error with connection with {1} (ID : {2}). Error code {0}", msg.errorCode, netMsg.conn.address, netMsg.conn.connectionId);
        }

        private void InitConnection(int id)
        {
            if (!iConnectedClientsWithData.Contains(id))
            {
                iConnectedClientsWithData.Add(id);
            }

            //if (!dictConnections1.ContainsKey(id))
            //{
            //    dictConnections1.Add(id, new SensorUnit());
            //}

            //if (!dictBodiesReceivedPerConnection1.ContainsKey(id))
            //{
            //    dictBodiesReceivedPerConnection1.Add(id, new List<KinectBody>(6));
            //}

            //if(!dictFacesReceivedPerConnection1.ContainsKey(id))
            //{
            //    dictFacesReceivedPerConnection1.Add(id, new List<FacialData>(6));
            //}

            if (_mainClientId == -1)
            {
                _mainClientId = id;
            }

            //UpdateConnection(id);

            //if (busyStreams1.Contains(id)) return;

            //busyStreams1.Add(id);
        }

        void UpdateConnection(int id)
        {
            //if (!iConnectedClientsWithData.Contains(id))
            //{
            //    iConnectedClientsWithData.Add(id);
            //}

            //if (!busyStreams1.Contains(id)) return;

            StopCoroutine(VerifyConnection(id));
            StartCoroutine(VerifyConnection(id));
        }

        void RemoveConnection(int id)
        {
            iConnectedClientsWithData.Remove(id);
            //dictConnections1.Remove(id);
            //dictBodiesReceivedPerConnection1.Remove(id);
            //dictFacesReceivedPerConnection1.Remove(id);
            //busyStreams1.Remove(id);
            //finishedStreams1.Remove(id);

            if (iConnectedClientsWithData.Count == 0)
                _mainClientId = -1;
        }

        IEnumerator VerifyConnection(int id)
        {
            //wait a little bit for data, should arrive +/- 30 times per sec, give or take a few connect deviations
            yield return new WaitForSeconds(.1f);
            NetworkServer.SendToClient(id, NetConstants.Ping, new EmptyMessage());

            //if pong is not received within 3 seconds, remove connection
            yield return new WaitForSeconds(3);
            Debug.Log("Timeout");
            RemoveConnection(id);
        }

        private void ProcessPong(NetworkMessage netMsg)
        {
            var id = netMsg.conn.connectionId;
            StopCoroutine(VerifyConnection(id));

            var val = netMsg.ReadMessage<IntegerMessage>().value;

            if (val == 0)       //slave no longer available
            {
                RemoveConnection(id);
            }
            else
                StartCoroutine(VerifyConnection(id));   //slave still available
        }

        long lastFrame;
        long thisFrame;

        DictData newestData;

        private void HandleDataStream(NetworkMessage netMsg)
        {
            //Debug.Log("Data received");
            var id = netMsg.conn.connectionId;

            var msg = netMsg.ReadMessage<KinectDataMessageCompressed>();

            if (_mainClientId == -1)
            {
                Debug.LogFormat("_mainClientId set to {0}", id);
                _mainClientId = id;
            }

            if (!iConnectedClientsWithData.Contains(id))
            {
                iConnectedClientsWithData.Add(id);
            }

            if (!allowMultipleSensors && _mainClientId != id)
            {
                netMsg.conn.Disconnect();
            }

            if (_handler == null)
            {
                _handler = new KinectDataBufferHandler();
            }

            _handler.PassData(id, new KinectData(msg));

            //newestData = _handler.GetData();

            //DeduceTransformatives(ref newestData);
            //lastFrame = thisFrame;
            //thisFrame = DateTime.Now.Ticks;

            //Debug.Log(thisFrame - lastFrame);

            //var data = _handler.GetData();

            //this.relativeTime = msg.relativeTime;

            //_hasFrame = TempCreateBody(msg);

            //if receive body, move to buffer, if buffer done, move to other buffer.
            //if receive body while processing, move to other buffer.

            //if (!mainBuffer.EndStream(id))
            //{
            //    if (!backBuffer.EndStream(id))
            //    {
            //        //buffer1.DumpData();
            //    }
            //}

            //ProcessFinalBodies();
        }

        List<int> removedIds = new List<int>();

        bool isTemp = true;    //small check that verifies if temporary features should be used, or if the actual program should be run

        //TODO : There's so much allocation during this function, there should be a way to optimize this process.
        void ProcessKinectData()
        {
            if (_finalKinectBodies == null || _handler == null || _mainClientId == -1)
            {
                return;
            }

            if (GUI != null)
            {
                GUI.text = string.Format("Main client : {0}", _mainClientId);
            }

            _finalKinectBodies.Clear();
            //get data
            DictData data = _handler.GetData();

            bool hasValidKeys = CheckKeyValidity(data);

            //do preliminary sweep
            CleanConnections();

            if (!hasValidKeys)
                return;

            int count = 0;
            {
                Debug.Log("Handling main client data");
                //add bodies from main id
                var kMsg = data[_mainClientId].data;

                //acquire main transform early to avoid repeated searches
                Transformative mainT = null;
                if (_kinectTransformations != null && _kinectTransformations.ContainsKey(_mainClientId))
                {
                    mainT = _kinectTransformations[_mainClientId];
                }


                for (int b = 0; b < kMsg.bodies.Length; ++b)
                {
                    if (kMsg.bodies[b] == null) continue;
                    ++count;
                    KinectBody k = kMsg.bodies[b].ToKBody();

                    _finalKinectBodies.Add(k.TransformLocToWorld(mainT));

                    if (!allowMultipleBodies)
                    {
                        Debug.Log("Only one body allowed");
                        break;
                    }
                }

                //if (kMsg.actualCount != count) Debug.LogError("Disparity found in bodies, pls verify");
                //Debug.LogFormat("BodyCount = {0}", count);
                GUI.text = string.Format("{0}\nbodies before appending: {1}", GUI.text, count);
            }
            hasFrame = (_finalKinectBodies.Count > 0);
            if (!allowMultipleSensors)
            {
                Debug.Log("Only one sensor allowed");
                CleanConnections();
                return;
            }

            //FixFacing(ref data);

            DeduceTransformatives(ref data);

            //for every id that's connected that's not the main
            if (FeatureManager.UseTempFeatures)
            {
                DoTempBodyStuff(ref data);
            }
            else
                DoBodyAppending(ref data);

            hasFrame = (_finalKinectBodies.Count > 0);

            GUI.text = string.Format("{0}\nbodies after appending: {1}", GUI.text, _finalKinectBodies.Count);


            //clean up
            CleanConnections();
        }

        private void DoBodyAppending(ref DictData data)
        {
            for (int i = 0; i < iConnectedClientsWithData.Count; ++i)
            {
                var id = iConnectedClientsWithData[i];

                //this is the one you can skip
                if (id == _mainClientId) continue;

                //if (!data.ContainsKey(id)) continue;
                Debug.LogFormat("Handling data from connection {0}", id);
                //if removed with obsoletion
                if (!data.ContainsKey(id))
                {
                    removedIds.Add(id);
                    continue;
                }

                //var mDat = data[_mainClientId];
                var kDat = data[id];

                AppendBodyData(id, ref kDat);
            }
        }

        private void AppendBodyData(int id, ref KinectData kinectData)
        {
            if (id == _mainClientId) return;

            var bodies = kinectData.data.bodies;
            for (int i = 0; i < bodies.Length; ++i)
            {
                bool bodyExists = false;
                if (bodies[i] == null) continue;
                for (int j = 0; j < _finalKinectBodies.Count; ++j)
                {
                    var main = _finalKinectBodies[j];

                    var sub = bodies[i];

                    if (BodiesNearlyOverlap(id, ref main, ref sub))
                    {
                        //do data appending
                        Debug.Log("Data appending done");
                        bodyExists = true;

                        //Main is always in world space, needs fixing.
                        AppendData(id, ref main, ref sub);
                    }
                    else continue;  //no data appending
                }

                if (!bodyExists
                    /* && allowMultipleBodies && _finalKinectBodies.Count == 0*/)
                {
                    //add to final bodies;
                    Debug.Log("Body added");
                    _finalKinectBodies.Add(bodies[i].ToKBody().TransformLocToWorld(_kinectTransformations[id]));
                }
                //if (!allowMultipleBodies) break;
            }
        }

        private void AppendData(int id, ref KinectBody mainB, ref KinectBodyMini subB)
        {
            for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++)
            {
                var m = mainB.Joints[jt];
                var s = subB.Joints[jt];
                AppendJoint(id, ref m, ref s);
                if (s.TrackingState > m.TrackingState)
                {
                    if (jt == JointType.SpineBase)
                    {
                        var mainT = _kinectTransformations[_mainClientId];
                        var subT = _kinectTransformations[id];

                        mainB.JointOrientations[jt].Orientation = subB.SpineBaseOrientation;
                    }
                    else if (jt == JointType.Neck)
                    {
                        var mainT = _kinectTransformations[_mainClientId];
                        var subT = _kinectTransformations[id];

                        mainB.JointOrientations[jt].Orientation = subB.NeckOrientation;
                    }
                }
            }
        }

        private void AppendData(Transformative mainT, ref KinectBody mainB,
                                    Transformative subT, ref KinectBodyMini subB)
        {

            for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++)
            {
                var m = mainB.Joints[jt];
                var s = subB.Joints[jt];
                AppendJoint(mainT, ref m, subT, ref s);
                if (s.TrackingState > m.TrackingState)
                {
                    if (jt == JointType.SpineBase)
                    {
                        mainB.JointOrientations[jt].Orientation = mainT.TransformRotLocToWorld(subB.SpineBaseOrientation);
                    }
                    else if (jt == JointType.Neck)
                    {
                        mainB.JointOrientations[jt].Orientation = subB.NeckOrientation;
                    }
                }
            }
        }

        private void AppendJoint(int id, ref KJoint main, ref KJoint sub)
        {
            if (sub.TrackingState > main.TrackingState) //if sub has better accuracy than main
            {
                var mainT = _kinectTransformations[_mainClientId];
                var subT = _kinectTransformations[id];

                main.Position = subT.TransformPosLocToWorld(sub.Position);
                main.PosPrevious = subT.TransformPosLocToWorld(sub.PosPrevious);
                main.PosRelative = subT.TransformPosLocToWorld(sub.PosRelative);
                main.PosDerivative = subT.TransformPosLocToWorld(sub.PosDerivative);
                main.TrackingState = sub.TrackingState;
            }
        }

        private void AppendJoint(Transformative mainT, ref KJoint main, Transformative subT, ref KJoint sub)
        {
            if (sub.TrackingState > main.TrackingState) //if sub has better accuracy than main
            {
                main.Position = subT.TransformPosLocToWorld(sub.Position);
                main.PosPrevious = subT.TransformPosLocToWorld(sub.PosPrevious);
                main.PosRelative = subT.TransformPosLocToWorld(sub.PosRelative);
                main.PosDerivative = subT.TransformPosLocToWorld(sub.PosDerivative);
                main.TrackingState = sub.TrackingState;
            }
        }

        private bool BodiesNearlyOverlap(int id, ref KinectBody mainBody,
                                                     ref KinectBodyMini comparedBody)
        {
            //var mainT = _kinectTransformations[_mainClientId];
            var compT = _kinectTransformations[id];
            if (mainBody.Joints[JointType.SpineBase].TrackingState == TrackingState.NotTracked ||
                comparedBody.Joints[JointType.SpineBase].TrackingState == TrackingState.NotTracked) return false;

            var mainSpineJoint = (mainBody.Joints[JointType.SpineBase].UPosKinect);
            var otherSpineJoint = compT.TransformPosLocToWorld(comparedBody.Joints[JointType.SpineBase].UPosKinect);

            return ((otherSpineJoint - mainSpineJoint).sqrMagnitude < 0.001f);


            //return false;
        }

        private void FixFacing(ref DictData data)       //checks the face quaternion, then fixes the rotation of the body.
        {
            for (int i = 0; i < iConnectedClientsWithData.Count; ++i)
            {
                var id = iConnectedClientsWithData[i];

                if (!data.ContainsKey(id)) continue;

                var kDat = data[id];
                FixFacing(ref kDat);
            }
        }

        private void FixFacing(ref KinectData kDat)
        {
            var data = kDat.data;

            for (int b = 0; b < 6; ++b)
            {
                //data[id].data.bodies[b];
                //if the conditions for the body look like the body is turned away from the camera, apply joint swapping
                var body = data.bodies[b];
                var faceQuat = data.faces[b];

                //invalid, can't deduce without face data, can't work without body data.
                if (body == null || !faceQuat.HasValue) continue;

                if (CheckTurned(ref body, ref faceQuat))
                {
                    //switch joint data
                    body.SwapAllJointsMicro();
                    //fix orientations
                    TrackingConfidence swap = body.HandLeftConfidence;
                    HandState s = body.HandLeftState;
                    body.HandLeftConfidence = body.HandRightConfidence;
                    body.HandLeftState = body.HandRightState;
                    body.HandRightConfidence = swap;
                    body.HandRightState = s;

                    body.SpineBaseOrientation = (Quaternion.Euler(0, 180, 0) * body.SpineBaseOrientation.ToUnityQuat()).ToKVec4();
                    body.NeckOrientation = (Quaternion.Euler(0, 180, 0) * body.NeckOrientation.ToUnityQuat()).ToKVec4();

                    Debug.Log("A body has been turned");
                }

            }
        }

        private bool CheckTurned(ref KinectBodyMini body, ref Windows.Kinect.Vector4? faceQuat)
        {
            //Based on the IsBodyTurned code from the Kinect2Interface
            //this is the comments provided:

            //face = On: Face (357.0/1.0)
            //face = Off
            //|   Head_px <= -0.02
            //|   |   Neck_dx <= 0.08: Face (46.0/1.0)
            //|   |   Neck_dx > 0.08: Back (3.0)
            //|   Head_px > -0.02
            //|   |   SpineShoulder_px <= -0.02: Face (4.0)
            //|   |   SpineShoulder_px > -0.02: Back (64.0/1.0)

            if (faceQuat.HasValue)   //face on
            {
                return false;
            }
            else
            {
                if (body.Joints[JointType.Head].PosRelative.x <= -0.02f)
                {
                    return (body.Joints[JointType.Head].PosDerivative.x > 0.08f);
                }
                else
                {
                    // Head_px > -0.02
                    return (body.Joints[JointType.Head].PosRelative.x > -0.02f);
                }
            }

            //return false;
        }

        //basically determine where the Kinect is in relation to the main Kinect, but I couldn't find a good name for it.
        private void DeduceTransformatives(ref DictData data)
        {
            //var time = DateTime.Now;
            //Debug.LogFormat("Start : {0}",time);

            if (_kinectTransformations == null)
                _kinectTransformations = new Dictionary<int, Transformative>();

            //can't properly deduce positions. quitting
            if (allowMultipleBodies)
            {
                Debug.Log("Multiple bodies not allowed, can't deduce transformatives");
                return;
            }



            for (int i = 0; i < iConnectedClientsWithData.Count; ++i)
            {
                var id = iConnectedClientsWithData[i];

                if (!data.ContainsKey(id))
                {
                    Debug.LogFormat("No data for ID : {0}", id);
                    continue;
                }

                if (!_kinectTransformations.ContainsKey(id))
                {
                    _kinectTransformations.Add(id, new Transformative());
                }

                if (id == _mainClientId)
                {
                    //_kinectTransformations[id].SetToZero();
                    continue; //no point in triangulating the main kinect with itself
                }

                if (!data.ContainsKey(_mainClientId))
                {
                    Debug.LogWarning("No data for main client id");
                    continue;
                }

                var mT = _kinectTransformations[_mainClientId];
                var mD = data[_mainClientId];
                var sT = _kinectTransformations[id];
                var sD = data[id];

                TriangulateTransform(ref mD, ref sD, ref mT, ref sT);
            }
            //Debug.LogFormat("Duration : {0}",DateTime.Now.Subtract(time));
        }

        private void TriangulateTransform(ref KinectData mainKData, ref KinectData secondKData,
                                                 ref Transformative mainKTransform, ref Transformative secondKTransform)
        {
            //get the important joints
            if (mainKData.data.bodies[0] == null || secondKData.data.bodies[0] == null)
            {
                Debug.Log("No bodies found, no determination");
                return;
            }

            var spineJointToMain = mainKData.data.bodies[0].Joints[JointType.SpineBase];
            var spineJointToTgt = secondKData.data.bodies[0].Joints[JointType.SpineBase];

            //if we can't track abandon
            if (spineJointToMain.TrackingState == TrackingState.NotTracked || spineJointToTgt.TrackingState == TrackingState.NotTracked)
            {
                Debug.Log("joints aren't tracked, no determination");
                return;
            }

            ////get main rotations
            //var spineRotToMain = mainKData.data.bodies[0].SpineBaseOrientation.ToUnityQuat();
            //var spineRotToTgt = secondKData.data.bodies[0].SpineBaseOrientation.ToUnityQuat();

            //Vector3 mainRotVector = spineRotToMain * Vector3.forward;   //local space main client tf
            //Vector3 tgtRotVector = spineRotToTgt * Vector3.forward; //this is local space

            var spineMainSecondJoint = mainKData.data.bodies[0].Joints[JointType.HipLeft];
            var spineTgtSecondJoint = secondKData.data.bodies[0].Joints[JointType.HipLeft];

            if (spineMainSecondJoint.TrackingState == TrackingState.NotTracked || spineTgtSecondJoint.TrackingState == TrackingState.NotTracked)
            {
                Debug.Log("joints aren't tracked, no determination");
                return;
            }

            Vector3 mainRotVector = (spineMainSecondJoint.UPosKinect - spineJointToMain.UPosKinect).normalized;
            Vector3 tgtRotVector = (spineTgtSecondJoint.UPosKinect - spineJointToTgt.UPosKinect).normalized;


            //main kinect vector has to be rotated to match its transform
            mainRotVector = Quaternion.Inverse(mainKTransform.Rotation) * mainRotVector;    //This is world space

            Quaternion RotBtwVecs = Quaternion.FromToRotation(tgtRotVector, mainRotVector); //difference from world to local space

            secondKTransform.Rotation = RotBtwVecs;

            secondKTransform.Position = mainKTransform.TransformPosLocToWorld(spineJointToMain.UPosKinect, false) - (secondKTransform.Rotation * spineJointToTgt.UPosKinect);

            //find a joint that has valid data, and triangulate that with the 
            //if (secondKData.data.bodies[0].Joints[JointType.SpineBase].TrackingState == TrackingState.Tracked)
            //{
            //    //ignore
            //    //toTriangulate.IsSure = true;
            //}

            var rotFwd = secondKTransform.Rotation * Vector3.forward;
            var pos = secondKTransform.Position;
            //Debug.LogFormat("Transformative triangulated, Rotation towards ({0},{1},{2}), Positioned at ({3},{4},{5})",
            //    rotFwd.x,rotFwd.y,rotFwd.z ,pos.x,pos.y,pos.z );

            //perform a small fault analysis
            secondKTransform.PositionFault = secondKTransform.TransformPosLocToWorld(spineJointToTgt.UPosKinect, false) - mainKTransform.TransformPosLocToWorld(spineJointToMain.UPosKinect, false);
            secondKTransform.RotationFault =
                Quaternion.Inverse( mainKTransform.TransformRotLocToWorld(mainKData.data.bodies[0].SpineBaseOrientation, false).ToUnityQuat() ) *
                secondKTransform.TransformRotLocToWorld(secondKData.data.bodies[0].SpineBaseOrientation, false).ToUnityQuat();
        }

        void OnDrawGizmos()
        {
            if (_kinectTransformations == null) return;
            var enumerator = _kinectTransformations.Values.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawWireSphere(enumerator.Current.Position, 3);
                Gizmos.DrawRay(enumerator.Current.Position, enumerator.Current.Rotation * Vector3.forward);
            }
        }

        private void CleanConnections()
        {
            for (int id = 0; id < removedIds.Count; ++id)
            {
                Debug.LogFormat("Connection removed : {0}", removedIds[id]);
                iConnectedClientsWithData.Remove(removedIds[id]);
            }
            removedIds.Clear();
        }

        private bool CheckKeyValidity(DictData data)
        {
            //data is null
            if (data == null)
            {
                Debug.LogWarning("No data has been passed");
                return false;
            }

            //data doesn't exist, presumably because all data has become obsolete.
            if (data.Count == 0)
            {
                Debug.LogWarning("No actual data, everything's obsolete");
                _mainClientId = -1;
                return false;
            }

            //data doesn't contain the main id data, due to obsoletion
            //take the first new data instead as main client id
            if (!data.ContainsKey(_mainClientId))
            {
                removedIds.Add(_mainClientId);
                var e = data.Keys.GetEnumerator();
                e.MoveNext();
                Debug.LogFormat("Switching main client id, former {0}, latter {1}", _mainClientId, e.Current);
                _mainClientId = e.Current;
                e.Dispose();
            }
            return true;
        }

        private void DoTempBodyStuff(ref DictData data)
        {
            for (int i = 0; i < iConnectedClientsWithData.Count; ++i)
            {
                var id = iConnectedClientsWithData[i];

                //this is the one you can skip
                if (id == _mainClientId) continue;

                //if (!data.ContainsKey(id)) continue;
                Debug.LogFormat("Handling data from connection {0}", id);
                //if removed with obsoletion
                if (!data.ContainsKey(id))
                {
                    removedIds.Add(id);
                    continue;
                }

                //var mDat = data[_mainClientId];
                var kDat = data[id];

                var bodies = kDat.data.bodies;
                for (int h = 0; h < bodies.Length; h++)
                {
                    if (bodies[h] == null) continue;
                    var k = bodies[h].ToKBody();
                    k.TrackingId += 256 * (ulong)(id);

                    if (_kinectTransformations == null)
                    {
                        Debug.LogWarning("Pls add transformatives");
                        _finalKinectBodies.Add(k);
                        CleanConnections();
                        return;
                    }

                    _finalKinectBodies.Add(k.TransformLocToWorld(_kinectTransformations[id]));
                }
            }

        }

        private bool TempCreateBody(KinectDataMessageCompressed msg)
        {
            bool hasBodies = false;

            for (int i = 0; i < 6; ++i)
            {
                if (msg.bodies[i] == null) continue;
                if (!hasBodies)
                {
                    _finalKinectBodies.Clear();
                    hasBodies = true;
                }


                //Debug.Log("body found");
                var msgB = msg.bodies[i];

                var b = new KinectBody();
                b.TrackingId = msgB.TrackingId;


                b.IsTracked = msgB.IsTracked;
                b.IsRestricted = msgB.IsRestricted;
                b.ClippedEdges = msgB.ClippedEdges;

                //Debug.LogFormat("Id : {0} - Tracked = {1} - Restricted = {2}", b.TrackingId, b.IsTracked, b.IsRestricted);

                b.HandLeftConfidence = msgB.HandLeftConfidence;
                b.HandRightConfidence = msgB.HandRightConfidence;
                b.HandLeftState = msgB.HandLeftState;
                b.HandRightState = msgB.HandRightState;

                b.JointOrientations[Windows.Kinect.JointType.SpineBase].JointType = Windows.Kinect.JointType.SpineBase;
                b.JointOrientations[Windows.Kinect.JointType.SpineBase].Orientation = msgB.SpineBaseOrientation;

                b.JointOrientations[Windows.Kinect.JointType.Head].JointType = Windows.Kinect.JointType.Head;
                if (msg.faces[i].HasValue)
                    b.JointOrientations[Windows.Kinect.JointType.Head].Orientation = msg.faces[i].Value;
                else
                {
                    //set to identity
                    var orient = new Windows.Kinect.Vector4();
                    orient.X = 0;
                    orient.Y = 0;
                    orient.Z = 0;
                    orient.W = 1;
                    b.JointOrientations[Windows.Kinect.JointType.Head].Orientation = orient;
                }

                for (Windows.Kinect.JointType jt = Windows.Kinect.JointType.SpineBase; jt <= Windows.Kinect.JointType.ThumbRight; ++jt)
                {
                    b.Joints[jt].TrackingState = msgB.Joints[jt].TrackingState;
                    b.Joints[jt].Position = msgB.Joints[jt].Position;
                    b.Joints[jt].JointType = jt;
                }

                _finalKinectBodies.Add(b);
            }
            return hasBodies;
        }

        //void SwapBuffers()
        //{
        //    //var temp = mainBuffer;
        //    //mainBuffer = backBuffer;
        //    //backBuffer = temp;
        //}

        public void AcquireLatestFinalBodies(ref List<KinectBody> bodyList, out long relativeTime)
        {
            //Check if a buffer is ready
            relativeTime = 0;
            //Debug.Log("Attempt at acquiring");

            ProcessKinectData();
            if (FeatureManager.UseTempFeatures)
            {
                hasFrame = false;
                DoTempStuff();
            }

            if (hasFrame)
            {
                //Debug.Log("Acquiring frame");
                hasFrame = false;
                if (_finalKinectBodies.Count == 0)
                {
                    Debug.LogWarning("No bodies found");
                    bodyList = null;
                    return;
                }
                relativeTime = DateTime.Now.Ticks;
                bodyList = new List<KinectBody>(_finalKinectBodies);
                Debug.LogFormat("Final kinect body count = {0}", _finalKinectBodies.Count);
            }
            else
            {
                //Debug.Log("No frame");
                bodyList = null;
            }
        }

        private void DoTempStuff()
        {
            //ProcessKinectData();
            for (int i = 0; i < _finalKinectBodies.Count; i++)
            {
                //visualize with blocks
                KinectBodyVisualizer.GetInstance_S().VisualizeBody(_finalKinectBodies[i]);
            }
        }

        //// Update is called once per frame
        //void Update()
        //{

        //}
    }

}

