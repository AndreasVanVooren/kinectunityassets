﻿using UnityEngine;
using System.Collections;

public class AutoRotate : MonoBehaviour
{

    public float Speed = 180;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward,Speed * Time.deltaTime);
    }
}
