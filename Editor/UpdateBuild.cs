﻿using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;
using System.Diagnostics;
using System.Collections.Generic;

public class UpdateBuild : EditorWindow {
    [MenuItem("Window/Build updater")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow<UpdateBuild>("Build Updater");
    }

	void OnGUI()
    {
        
        if(GUILayout.Button("Build and push to git"))
        {
            BuildAndPushBuild();
        }
    }

    private static void BuildAndPushBuild()
    {
        List<string> scenes = new List<string>();
        for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
        {
            scenes.Add(EditorBuildSettings.scenes[i].path);
        }

        var errors = BuildPipeline.BuildPlayer(scenes.ToArray(), "../KinectBuild/kinectNet.exe", EditorUserBuildSettings.activeBuildTarget, BuildOptions.Development | BuildOptions.AllowDebugging);
        while (BuildPipeline.isBuildingPlayer) ;
        if (errors == "")
        {
            Debug.Log("done");
            Process.Start("C:/Batch/push.bat");
        }
    }
}
